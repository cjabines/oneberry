package com.groupm.labs;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by zechariah.jabines on 1/26/16.
 */
public class LocalAppData {
    private static LocalAppData sLocalAppData = null;
    private Context mAppContext;


    public LocalAppData(Context context) {
        super();
        mAppContext = context;
    }

    public static LocalAppData getLocalData(Context context) {
        if(sLocalAppData == null) {
            sLocalAppData = new LocalAppData(context);
        }
        return sLocalAppData;
    }

    public void createNewSharedPreference(String prefKey) {
        // we'll check first if we already have this prekey
        if(mAppContext.getSharedPreferences(prefKey, Context.MODE_PRIVATE) == null) {
            mAppContext.getSharedPreferences(prefKey, Context.MODE_PRIVATE);
        }else{

        }
    }

    public boolean hasKey(String key, String prefKey) {
        SharedPreferences sharedPreferences = mAppContext.getSharedPreferences(prefKey, Context.MODE_PRIVATE);
        return sharedPreferences.contains(key);
    }

    public String getValueForKey(String key, String prefKey) {
        SharedPreferences sharedPreferences = mAppContext.getSharedPreferences(prefKey, Context.MODE_PRIVATE);
        return sharedPreferences.getString(key, null);
    }

    public void setValueForKey(String prefKey, String key, String value) {
        SharedPreferences.Editor editor = mAppContext.getSharedPreferences(prefKey, Context.MODE_PRIVATE).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void deleteEntryFromSharedKey(String prefKey, String key) {
        SharedPreferences.Editor editor = mAppContext.getSharedPreferences(prefKey, Context.MODE_PRIVATE).edit();
        editor.remove(key).commit();
        editor.apply();
    }

    public void deleteAllEntryFromSharedKey(String prefKey) {
        SharedPreferences.Editor editor = mAppContext.getSharedPreferences(prefKey, Context.MODE_PRIVATE).edit();
        editor.clear().commit();
        editor.apply();
    }
}
