package com.oneberry.oneberryapp.data;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by zechariah.jabines on 3/22/16.
 */
public class APIManager {
    private static final String  BASE_URL = "http://42.61.98.65:82/mgt";

    public static String login(String username, String password, String imei) {
        /*try {
            String un = URLEncoder.encode(username, "utf-8");
            String pw = URLEncoder.encode(password, "utf-8");
            String im = URLEncoder.encode(imei, "utf-8");
            return BASE_URL+"/login.php?username="+un+"&password="+pw+"&imei="+im;
        } catch (UnsupportedEncodingException e) {}*/
        return BASE_URL+"/login.php";
    }

    // USER
    public static final String GET_USER_LIST = BASE_URL+"/appGetAllUsers.php";
    public static final String GET_ARCHIVED_USER = BASE_URL+"/appGetAllArchivedUsers.php";

    public static String searchUser(String searchkey) {
        return BASE_URL+"/appGetSearchUsers.php?searchKey="+searchkey;
    }

    public static String searchArchivedUser(String searchkey) {
        return BASE_URL+"/appGetSearchArchivedUsers.php?searchKey="+searchkey;
    }

    // ACCOUNT
    public static final String GET_ACCOUNT_LIST = BASE_URL+"/appGetAccounts.php";
    public static final String GET_ARCHIVED_ACCOUNT = BASE_URL+"/appGetArchivedAccounts.php";

    public static String searchAccount(String searchkey) {
        return BASE_URL+"/appSearchAccounts.php?searchKey="+searchkey;
    }

    public static String searchArchivedAccount(String searchkey) {
        return BASE_URL+"/appSearchArchivedAccounts.php?searchKey="+searchkey;
    }

    // SITES
    public static final String GET_SITE_LIST = BASE_URL+"/appGetSites.php";
    public static final String GET_ARCHIVED_SITE = BASE_URL+"/appGetArchivedSites.php";

    public static String searchSite(String searchkey) {
        return BASE_URL+"/appSearchSites.php?searchKey="+searchkey;
    }

    public static String searchArchivedSite(String searchkey) {
        return BASE_URL + "/appSearchArchivedSites.php?searchKey=" + searchkey;
    }

    // CHECKPOINTS
    public static final String GET_CHECKPOINT_LIST = BASE_URL+"/appGetCheckpoints.php";
    public static final String GET_ARCHIVED_CHECKPOINT = BASE_URL+"/appGetArchivedCheckpoints.php";

    // TOUR
    public static final String GET_TOURS_LIST = BASE_URL+"/appGetTours.php";
    public static String reportCheckpoint(String userID, String tagID, String imei) {
        try {
            String un = URLEncoder.encode(userID, "utf-8");
            String tag = URLEncoder.encode(tagID, "utf-8");
            String im = URLEncoder.encode(imei, "utf-8");
            return BASE_URL+"/reportCheckpoint.php?userID="+un+"&tagID="+tag+"&imei="+im;
        } catch (UnsupportedEncodingException e) {}
        //return BASE_URL+"/reportCheckpoint.php";
        return BASE_URL+"/reportCheckpoint.php?userID="+userID+"&tagID="+tagID+"&imei="+imei;
    }

    // EVENT
    public static String reportEvent(String dataString, String userID, String reportid) {
        /*try {
            String un = URLEncoder.encode(userID, "utf-8");
            String tag = URLEncoder.encode(tagID, "utf-8");
            String im = URLEncoder.encode(imei, "utf-8");
            return BASE_URL+"/reportCheckpoint.php?userID="+un+"&tagID="+tag+"&imei="+im;
        } catch (UnsupportedEncodingException e) {}
        //return BASE_URL+"/reportCheckpoint.php";*/
        return BASE_URL+"/reportEvent.php";
    }



    public static String userID="";
    public static String userType="";
    public static String userIMEI = "";
    public static String userCanScan = "";
}
