package com.oneberry.oneberryapp.services;

import android.app.IntentService;
import android.content.Intent;
import android.os.CountDownTimer;

import com.oneberry.oneberryapp.controller.HomeActivity;
import com.oneberry.oneberryapp.controller.fragments.home.TourListFragment;

/**
 * Created by zechariah.jabines on 5/6/16.
 */
public class TimerServices extends IntentService {

    private String TAG = "TimerServices";
    private Intent homeActivityIntent;
    private CountDownTimer countDownTimer;
    public static final String EXTRA_DIALOG = "EXTRA_DIALOG";


    public TimerServices() {
        super("");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        int timer = intent.getIntExtra(TourListFragment.EXTRA_TIME, 0);
        countDownTimer = new CountDownTimer(timer, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                homeActivityIntent = new Intent(TimerServices.this, HomeActivity.class);
                homeActivityIntent.putExtra(EXTRA_DIALOG, true);
                startActivity(homeActivityIntent);
                countDownTimer.cancel();
            }
        };
        countDownTimer.start();
        return START_STICKY;
    }
}
