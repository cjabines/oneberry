package com.oneberry.oneberryapp.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by zechariah.jabines on 3/15/16.
 */
public class Account {

    private Date dateAdded;
    private String accountID;
    private String companyName;

    private String address1;
    private String address2;
    private String state;
    private String country;
    private String zipCode;

    private String billingAddress1;
    private String billingAddress2;
    private String billingState;
    private String billingCountry;
    private String billingZipCode;
    private String billingNotes;
    private JSONArray accountContactsJSONArray;

    private int accountIndex;

    public Account() {
        super();
        accountContactsJSONArray = new JSONArray();
    }

    public int getAccountIndex() {
        return accountIndex;
    }

    public void setAccountIndex(int accountIndex) {
        this.accountIndex = accountIndex;
    }

    public void setDateAdded(Date dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Date getDateAdded() {
        return dateAdded;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getBillingAddress1() {
        return billingAddress1;
    }

    public void setBillingAddress1(String billingAddress1) {
        this.billingAddress1 = billingAddress1;
    }

    public String getBillingAddress2() {
        return billingAddress2;
    }

    public void setBillingAddress2(String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    public String getBillingState() {
        return billingState;
    }

    public void setBillingState(String billingState) {
        this.billingState = billingState;
    }

    public String getBillingCountry() {
        return billingCountry;
    }

    public void setBillingCountry(String billingCountry) {
        this.billingCountry = billingCountry;
    }

    public String getBillingZipCode() {
        return billingZipCode;
    }

    public void setBillingZipCode(String billingZipCode) {
        this.billingZipCode = billingZipCode;
    }

    public String getBillingNotes() {
        return billingNotes;
    }

    public void setBillingNotes(String billingNotes) {
        this.billingNotes = billingNotes;
    }

    public void setAccountContacts(JSONArray jsonArray) {
        accountContactsJSONArray = jsonArray;
    }

    public ArrayList<AccountContact> getAccountContacts() {
        ArrayList<AccountContact> contacts = new ArrayList<>();

        try {
            for(int i = 0; i < accountContactsJSONArray.length(); i++) {
                AccountContact accountContact = new AccountContact();
                accountContact.setFirstName(accountContactsJSONArray.getJSONObject(i).getString("firstname"));
                accountContact.setLastName(accountContactsJSONArray.getJSONObject(i).getString("lastname"));
                accountContact.setEmail(accountContactsJSONArray.getJSONObject(i).getString("email"));
                accountContact.setMobile(accountContactsJSONArray.getJSONObject(i).getString("mobile"));
                contacts.add(accountContact);
            }
            return contacts;
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public JSONObject generateJSON() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("date", dateAdded);
            jsonObject.put("accountID", accountID);
            jsonObject.put("companyName", companyName);

            jsonObject.put("address1", address1);
            jsonObject.put("address2", address2);
            jsonObject.put("state", state);
            jsonObject.put("country", country);
            jsonObject.put("zip", zipCode);

            jsonObject.put("billAddress1", billingAddress1);
            jsonObject.put("billAddress2", billingAddress2);
            jsonObject.put("billState", billingState);
            jsonObject.put("billCountry", billingCountry);
            jsonObject.put("billZip", billingCountry);
            jsonObject.put("billNotes", billingNotes);

            jsonObject.put("accountContact", accountContactsJSONArray);
            return jsonObject;
        }catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }
}
