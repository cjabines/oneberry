package com.oneberry.oneberryapp.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zechariah.jabines on 3/15/16.
 */
public class AccountContact {

    private String firstName;
    private String lastName;
    private String designation;
    private String email;
    private String mobile;
    private String directLine;
    private String otherPhone;
    private String fax;
    private String skype;
    private String others;
    private String notes;
    private int contactIndex;

    public AccountContact() {
        super();
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDesignation() {
        return designation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDirectLine() {
        return directLine;
    }

    public void setDirectLine(String directLine) {
        this.directLine = directLine;
    }

    public void setOtherPhone(String otherPhone) {
        this.otherPhone = otherPhone;
    }

    public String getOtherPhone() {
        return otherPhone;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFax() {
        return fax;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getSkype() {
        return skype;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getOthers() {
        return others;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public JSONObject generateJSON() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("firstname", firstName);
            jsonObject.put("lastname", lastName);
            jsonObject.put("designation", designation);
            jsonObject.put("email", email);
            jsonObject.put("mobile", mobile);
            jsonObject.put("direct", directLine);
            jsonObject.put("otherphone", otherPhone);
            jsonObject.put("fax", fax);
            jsonObject.put("skype", skype);
            jsonObject.put("others", others);
            jsonObject.put("notes", notes);

            return jsonObject;
        }catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public int getContactIndex() {
        return contactIndex;
    }

    public void setContactIndex(int contactIndex) {
        this.contactIndex = contactIndex;
    }
}
