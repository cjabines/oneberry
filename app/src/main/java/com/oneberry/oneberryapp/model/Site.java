package com.oneberry.oneberryapp.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

/**
 * Created by zechariah.jabines on 3/15/16.
 */
public class Site {

    private String dateAdded;
    private String siteID;
    private String accountID;
    private String companyName;
    private String siteName;
    private String siteType;

    private String address1;
    private String address2;
    private String state;
    private String country;
    private String zipCode;
    private String others;
    private String notes;

    private int siteIndex;

    private JSONArray siteContactsJSONArray;

    public Site() {
        super();
        siteContactsJSONArray = new JSONArray();
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public String getsiteID() {
        return siteID;
    }

    public void setsiteID(String siteID) {
        this.siteID = siteID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public String getSiteType() {
        return siteType;
    }

    public void setSiteType(String siteType) {
        this.siteType = siteType;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    private void setSiteContacts(AccountContact accountContact) {
        siteContactsJSONArray.put(accountContact.generateJSON());
    }

    public JSONArray getSiteContacts() {
        return siteContactsJSONArray;
    }

    public JSONObject generateJSON() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("date", dateAdded);
            jsonObject.put("siteID", siteID);
            jsonObject.put("companyName", companyName);

            jsonObject.put("address1", address1);
            jsonObject.put("address2", address2);
            jsonObject.put("state", state);
            jsonObject.put("country", country);
            jsonObject.put("zip", zipCode);

            jsonObject.put("accountContact", siteContactsJSONArray);
            return jsonObject;
        }catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getAccountID() {
        return accountID;
    }

    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public int getSiteIndex() {
        return siteIndex;
    }

    public void setSiteIndex(int siteIndex) {
        this.siteIndex = siteIndex;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
