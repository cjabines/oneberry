package com.oneberry.oneberryapp.model.managers;

import android.content.Context;

import com.oneberry.oneberryapp.model.Account;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/15/16.
 */
public class AccountsManager {

    private ArrayList<Account> mAccountsArrayList = new ArrayList<>();
    private static AccountsManager sAccountsManager;
    private Context context;


    public AccountsManager(Context context) {
        super();

        this.context = context;
    }

    public static AccountsManager getInstance(Context context) {
        if(sAccountsManager == null) {
            sAccountsManager = new AccountsManager(context);
        }
        return sAccountsManager;
    }


    public void addAccount(Account account) {
        mAccountsArrayList.add(account);
    }

    public ArrayList<Account> getAccountList() {
        return mAccountsArrayList;
    }

    public void removeAllData() {
        mAccountsArrayList.clear();
    }
}
