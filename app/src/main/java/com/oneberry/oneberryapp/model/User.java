package com.oneberry.oneberryapp.model;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zechariah.jabines on 3/15/16.
 */
public class User {

    private String userID;
    private String userType;
    private String userName;
    private String password;
    private String firstName;
    private String lastName;
    private String designation;
    private String emailAddress;
    private String mobileNumber;
    private String directLine;
    private String directLineExt;
    private String otherPhone;
    private String fax;
    private String skype;
    private String others;
    private String notes;
    private String dateAdded;
    private int userIndex;

    public User() {
        super();
    }

    public int getUserIndex() {
        return userIndex;
    }

    public void setUserIndex(int userIndex) {
        this.userIndex = userIndex;
    }

    public void setDirectLineExt(String directLineExt) {
        this.directLineExt = directLineExt;
    }

    public String getDirectLineExt() {
        return directLineExt;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserType() {
        return userType;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getDirectLine() {
        return directLine;
    }

    public void setDirectLine(String directLine) {
        this.directLine = directLine;
    }

    public String getOtherPhone() {
        return otherPhone;
    }

    public void setOtherPhone(String otherPhone) {
        this.otherPhone = otherPhone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public JSONObject generateJSONData() {
        try {
            JSONObject userJSONObject = new JSONObject();
            userJSONObject.put("usertype", userType);
            userJSONObject.put("username", userName);
            userJSONObject.put("password", password);
            userJSONObject.put("firstName", firstName);
            userJSONObject.put("lastName", lastName);
            userJSONObject.put("designation", designation);
            userJSONObject.put("email", emailAddress);
            userJSONObject.put("mobile", mobileNumber);
            userJSONObject.put("directline", directLine);
            userJSONObject.put("otherPhone", otherPhone);
            userJSONObject.put("fax", fax);
            userJSONObject.put("skype", skype);
            userJSONObject.put("other", others);
            userJSONObject.put("notes", notes);

            return userJSONObject;
        }catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
