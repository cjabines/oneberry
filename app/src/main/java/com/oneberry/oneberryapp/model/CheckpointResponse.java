package com.oneberry.oneberryapp.model;

/**
 * Created by zechariah.jabines on 4/6/16.
 */
public class CheckpointResponse {

    private String isSuccess;
    private String isNewCheckpoint;
    private String location;
    private String checkpointID;
    private String reply;
    private String finished;
    private String duration;

    public CheckpointResponse() {
        super();
    }

    public String getIsSuccess() {
        return isSuccess;
    }

    public void setIsSuccess(String isSuccess) {
        this.isSuccess = isSuccess;
    }

    public String getIsNewCheckpoint() {
        return isNewCheckpoint;
    }

    public void setIsNewCheckpoint(String isNewCheckpoint) {
        this.isNewCheckpoint = isNewCheckpoint;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCheckpointID() {
        return checkpointID;
    }

    public void setCheckpointID(String checkpointID) {
        this.checkpointID = checkpointID;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getFinished() {
        return finished;
    }

    public void setFinished(String finished) {
        this.finished = finished;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
