package com.oneberry.oneberryapp.model.managers;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 5/25/16.
 */
public class CheckpointIDManager {

    private Context context;
    private static CheckpointIDManager sCheckpointIDManager;
    private ArrayList<String> idStrings = new ArrayList<>();

    public CheckpointIDManager(Context context) {
        super();
        this.context = context;
        idStrings.add(0, "Select Checkpoint (ID)");
    }

    public static CheckpointIDManager get(Context context) {
        if(sCheckpointIDManager == null) {
            sCheckpointIDManager = new CheckpointIDManager(context);
        }
        return sCheckpointIDManager;
    }

    public void addIDs(String id) {
        idStrings.add(id);
    }

    public ArrayList<String> getIds() {
        return this.idStrings;
    }

    public void clearIDs() {
        this.idStrings.clear();
    }
}