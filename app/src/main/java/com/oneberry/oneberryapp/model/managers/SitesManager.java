package com.oneberry.oneberryapp.model.managers;

import android.content.Context;

import com.oneberry.oneberryapp.model.Site;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/15/16.
 */
public class SitesManager {

    private ArrayList<Site> mSitesArrayList = new ArrayList<>();
    private static SitesManager sSitesManager;
    private Context context;


    public SitesManager(Context context) {
        super();

        this.context = context;
    }

    public static SitesManager getInstance(Context context) {
        if(sSitesManager == null) {
            sSitesManager = new SitesManager(context);
        }
        return sSitesManager;
    }


    public void addSites(Site site) {
        mSitesArrayList.add(site);
    }

    public ArrayList<Site> getSiteList() {
        return mSitesArrayList;
    }


    public void removeAllData() {
        mSitesArrayList.clear();
    }
}
