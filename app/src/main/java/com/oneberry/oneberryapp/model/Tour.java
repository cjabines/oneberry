package com.oneberry.oneberryapp.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by zechariah.jabines on 3/15/16.
 */
public class Tour {

    private String tourID;
    private String dateAdded;
    private String guardTourName;
    private String tourType;
    private String days;
    private String startTime;
    private String endTime;
    private String notes;
    private int index;
    private JSONArray tourCheckpoints;

    public Tour() {
        super();
    }


    public String getTourID() {
        return tourID;
    }

    public void setTourID(String tourID) {
        this.tourID = tourID;
    }

    public String getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(String dateAdded) {
        this.dateAdded = dateAdded;
    }

    public String getGuardTourName() {
        return guardTourName;
    }

    public void setGuardTourName(String guardTourName) {
        this.guardTourName = guardTourName;
    }

    public String getTourType() {
        return tourType;
    }

    public void setTourType(String tourType) {
        this.tourType = tourType;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public JSONArray getTourCheckpoints() {
        return tourCheckpoints;
    }

    public void setTourCheckpoints(JSONArray tourCheckpoints) {
        this.tourCheckpoints = tourCheckpoints;
    }

    public ArrayList<Checkpoint> getTourCheckpointList() {
        ArrayList<Checkpoint> checkpoints = new ArrayList<>();
        try {
            JSONArray jsonArray = getTourCheckpoints();
            int length = jsonArray.length();
            for(int i = 0; i < length; i++) {
                JSONObject jObject = jsonArray.getJSONObject(i);
                Checkpoint checkpoint = new Checkpoint();
                checkpoint.setCheckpointIndex(i);
                checkpoint.setBranch(jObject.getString("branch"));
                checkpoint.setCheckpointName(jObject.getString("checkpointName"));
                checkpoint.setCheckpointType(jObject.getString("checkpointType"));
                checkpoint.setTagID(jObject.getString("tagid"));
                checkpoint.setLocation(jObject.getString("location"));
                checkpoint.setCoordinates(jObject.getString("coordinates"));
                checkpoint.setNotes(jObject.getString("notes"));
                checkpoints.add(checkpoint);
            }
            return checkpoints;
        }catch (JSONException e) {
            e.printStackTrace();
        }

        return checkpoints;
    }
}
