package com.oneberry.oneberryapp.model.managers;

import android.content.Context;
import android.util.Log;

import com.oneberry.oneberryapp.model.User;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/15/16.
 */
public class UsersManager {

    private ArrayList<User> mUserArrayList = new ArrayList<>();
    private static UsersManager sUserManager;
    private Context context;


    public UsersManager(Context context) {
        super();

        this.context = context;
    }

    public static UsersManager getInstance(Context context) {
        if(sUserManager == null) {
            sUserManager = new UsersManager(context);
        }
        return sUserManager;
    }


    public void addUser(User user) {
        Log.d("addUser", "addUser: " + user.getFirstName());
        mUserArrayList.add(user);
    }

    public ArrayList<User> getUserList() {
        return mUserArrayList;
    }

    public void removeAllData() {
        mUserArrayList.clear();
    }
}
