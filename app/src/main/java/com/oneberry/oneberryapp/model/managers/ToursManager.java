package com.oneberry.oneberryapp.model.managers;

import android.content.Context;
import android.util.Log;

import com.oneberry.oneberryapp.model.Tour;
import com.oneberry.oneberryapp.model.User;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/15/16.
 */
public class ToursManager {

    private ArrayList<Tour> mTourArrayList = new ArrayList<>();
    private static ToursManager sTourManager;
    private Context context;


    public ToursManager(Context context) {
        super();
        this.context = context;
    }

    public static ToursManager getInstance(Context context) {
        if(sTourManager == null) {
            sTourManager = new ToursManager(context);
        }
        return sTourManager;
    }


    public void addTour(Tour tour) {
        mTourArrayList.add(tour);
    }

    public ArrayList<Tour> getTourList() {
        return mTourArrayList;
    }

    public void removeAllData() {
        mTourArrayList.clear();
    }
}
