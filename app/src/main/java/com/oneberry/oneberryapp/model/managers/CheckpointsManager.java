package com.oneberry.oneberryapp.model.managers;

import android.content.Context;

import com.oneberry.oneberryapp.model.Checkpoint;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/15/16.
 */
public class CheckpointsManager {

    private ArrayList<Checkpoint> mCheckpointsArrayList = new ArrayList<>();
    private static CheckpointsManager sCheckpointsManager;
    private Context context;


    public CheckpointsManager(Context context) {
        super();

        this.context = context;
    }

    public static CheckpointsManager getInstance(Context context) {
        if(sCheckpointsManager == null) {
            sCheckpointsManager = new CheckpointsManager(context);
        }
        return sCheckpointsManager;
    }


    public void addCheckpoint(Checkpoint checkpoint) {
        mCheckpointsArrayList.add(checkpoint);
    }

    public void addCheckpointList(ArrayList<Checkpoint> arrayList) {
        mCheckpointsArrayList = arrayList;
    }

    public ArrayList<Checkpoint> getCheckpointList() {
        return mCheckpointsArrayList;
    }

    public void removeAllData() {
        mCheckpointsArrayList.clear();
    }
}
