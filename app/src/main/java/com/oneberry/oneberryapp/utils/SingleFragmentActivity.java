package com.oneberry.oneberryapp.utils;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.oneberry.oneberryapp.R;


/**
 * Created by zechariah.jabines on 1/20/16.
 */
public abstract class SingleFragmentActivity extends AppCompatActivity {

    protected abstract Fragment createFragment();

    protected int getLayoutId() {
        return R.layout.activity_fragment;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.frame_container);

        if(fragment == null) {
            fragment = createFragment();
            fragment.setHasOptionsMenu(true);
            fragment.setRetainInstance(true);
            fragmentManager.beginTransaction().add(R.id.frame_container, fragment).commit();
        }
    }
}