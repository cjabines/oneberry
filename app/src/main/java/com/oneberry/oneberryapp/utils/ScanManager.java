package com.oneberry.oneberryapp.utils;

import android.content.Context;

/**
 * Created by zechariah.jabines on 4/12/16.
 */
public class ScanManager {
    private static ScanManager ourInstance;
    private Context context;

    private ScanManager(Context context) {
        this.context = context;
    }

    public static ScanManager getInstance(Context context) {
        if(ourInstance == null) {
            ourInstance = new ScanManager(context);
        }
        return  ourInstance;
    }


}
