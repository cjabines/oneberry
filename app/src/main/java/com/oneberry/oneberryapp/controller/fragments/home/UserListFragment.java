package com.oneberry.oneberryapp.controller.fragments.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.groupm.labs.HttpResponseCache;
import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.adapter.UserListRecyclerAdapter;
import com.oneberry.oneberryapp.data.APIManager;
import com.oneberry.oneberryapp.model.User;
import com.oneberry.oneberryapp.model.managers.UsersManager;
import com.oneberry.oneberryapp.utils.MyTypeface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/21/16.
 */
public class UserListFragment extends Fragment {

    TextView mContentInfoTextView;
    RecyclerView mUserListRecyclerView;
    LinearLayoutManager mLinearLayoutManager;

    // data
    private UsersManager mUsersManager;
    private UserListRecyclerAdapter mUserListRecyclerAdapter;
    private HttpResponseCache mDataLoader;
    private Spinner mFilterSpinner;
    private String mUrlString;

    // progress
    private ProgressDialog mProgressDialog;


    private static final String TAG = "UserListFragment";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUsersManager = UsersManager.getInstance(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_list_fragment, container, false);
        // progress dialog
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading Active Users");

        // textview
        mContentInfoTextView = (TextView)view.findViewById(R.id.info_textView);
        mContentInfoTextView.setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));
        mContentInfoTextView.setText("Loading Users..");

        // set recycler view
        mUserListRecyclerView = (RecyclerView)view.findViewById(R.id.user_list);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mUserListRecyclerView.setLayoutManager(mLinearLayoutManager);

        // set adapter
        mUserListRecyclerAdapter = new UserListRecyclerAdapter(mUsersManager.getUserList());
        mUserListRecyclerView.setAdapter(mUserListRecyclerAdapter);

        // set filter
        // set spinner content
        mFilterSpinner = (Spinner)view.findViewById(R.id.filter_spinner);
        ArrayList<String> filterList = new ArrayList<>();
        filterList.add("Active");
        filterList.add("Archived");
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(), filterList);
        mFilterSpinner.setAdapter(adapter);

        mFilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mContentInfoTextView.setText("Loading Users");

                if(position == 0) {
                    mUsersManager.removeAllData();
                    mUserListRecyclerAdapter.notifyDataSetChanged();

                    mUrlString = APIManager.GET_USER_LIST;
                    loadUserData();
                    mProgressDialog.setMessage("Loading Active Users");
                    mProgressDialog.show();
                }else{
                    // remove data
                    mUsersManager.removeAllData();
                    mUserListRecyclerAdapter.notifyDataSetChanged();

                    mUrlString = APIManager.GET_ARCHIVED_USER;
                    loadUserData();
                    mProgressDialog.setMessage("Loading Archived Users");
                    mProgressDialog.show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

    private void loadUserData() {
        // set data loader
        mDataLoader = new HttpResponseCache(getActivity(), mUrlString, "");
        mDataLoader.setWillAddToCache(false);
        mDataLoader.setObjectListener(new HttpResponseCache.OnDataLoaderListener() {
            @Override
            public void onDataLoadComplete(JSONObject jsonObject) {
                try {
                    JSONArray userArrray = jsonObject.getJSONArray("data");
                    int jsonLength = userArrray.length();

                    mContentInfoTextView.setText("Number of Users: " + jsonLength);

                    for(int i = 0; i < jsonLength; i++) {
                        JSONObject userObject = userArrray.getJSONObject(i);
                        User user = new User();
                        user.setUserIndex(i);
                        user.setOthers(userObject.getString("others"));
                        user.setFax(userObject.getString("fax"));
                        user.setOtherPhone(userObject.getString("otherPhoneExt"));
                        user.setDesignation(userObject.getString("designation"));
                        user.setUserID(userObject.getString("userid"));
                        user.setEmailAddress(userObject.getString("emailAddress"));
                        user.setLastName(userObject.getString("lastname"));
                        user.setDateAdded(userObject.getString("dateAdded"));
                        user.setOtherPhone(userObject.getString("otherPhone"));
                        user.setFirstName(userObject.getString("firstname"));
                        user.setUserType(userObject.getString("userType"));
                        user.setUserName(userObject.getString("username"));
                        user.setDirectLine(userObject.getString("directLine"));
                        user.setDirectLineExt(userObject.getString("directLineExt"));
                        user.setNotes(userObject.getString("notes"));
                        user.setMobileNumber(userObject.getString("mobileNo"));
                        user.setSkype(userObject.getString("skype"));
                        mUsersManager.addUser(user);
                    }
                    mUserListRecyclerAdapter.notifyDataSetChanged();
                    mProgressDialog.dismiss();
                }catch(JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDataLoadFailed() {

            }

            @Override
            public void onCanceled() {

            }
        });
        mDataLoader.execute();
    }

    public void searchUser(String searchKey) {
        mUsersManager.removeAllData();
        mUserListRecyclerAdapter.notifyDataSetChanged();
        mProgressDialog.setMessage("Searching: " + searchKey);
        mProgressDialog.show();
        if(mFilterSpinner.getSelectedItemPosition() == 0) {
            mUrlString = APIManager.searchUser(searchKey);
        }else{
            mUrlString = APIManager.searchArchivedUser(searchKey);
        }
        loadUserData();
    }

    private class CustomSpinnerAdapter extends BaseAdapter {
        ArrayList<String> stringArrayList = new ArrayList<>();
        Context context;

        public CustomSpinnerAdapter(Context context, ArrayList<String> arrayList) {
            this.context = context;
            this.stringArrayList = arrayList;
        }

        @Override
        public int getCount() {
            return stringArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if(view == null) {
                // we'll create a custom view
                view = LayoutInflater.from(context).inflate(R.layout.spinner_basic_layout, parent, false);

                TextView spinnerTextView = (TextView)view.findViewById(R.id.spinner_textview);
                spinnerTextView.setText(stringArrayList.get(position));
                spinnerTextView.setTypeface(MyTypeface.get(context, "clan-book-webfont"));
            }

            return view;
        }
    }
}
