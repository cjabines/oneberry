package com.oneberry.oneberryapp.controller;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.groupm.labs.HttpResponseCache;
import com.groupm.labs.LocalAppData;
import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.data.APIManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zechariah.jabines on 3/15/16.
 */
public class LoginFragment extends Fragment {

    EditText mUserName;
    EditText mPassword;
    TextView mDescription;
    Button mLoginButton;
    HttpResponseCache mLoginResponseCache;
    ProgressDialog mProgressDialog;
    TextInputLayout mUsernameTIL, mPasswordTIL;
    LocalAppData mLocalAppData;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocalAppData = LocalAppData.getLocalData(getContext());
        mLocalAppData.createNewSharedPreference("login_oneberry");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.login_fragment, container, false);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Logging in. Please wait");

        // FULLSCREEN SPLASH PAGE
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // bind ui
        mUsernameTIL = (TextInputLayout)view.findViewById(R.id.til_username);
        mPasswordTIL = (TextInputLayout)view.findViewById(R.id.til_password);

        mUserName = (EditText)view.findViewById(R.id.username);
        mPassword = (EditText)view.findViewById(R.id.password);
        mDescription = (TextView)view.findViewById(R.id.user_description_textView);
        mLoginButton = (Button)view.findViewById(R.id.login);


        mUsernameTIL.setErrorEnabled(true);
        mPasswordTIL.setErrorEnabled(true);

        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean login = false;
                if(mUserName.getText().length() < 1) {
                    login = false;
                    mUsernameTIL.setError("Missing field: Username");
                }

                if(mPassword.getText().length() < 1) {
                    login = false;
                    mPasswordTIL.setError("Missing field: Password");
                }

                if(mPassword.getText().length() > 1 && mUserName.getText().length() > 1) {
                    login = true;
                    mPasswordTIL.setError("");
                    mUsernameTIL.setError("");
                }

                if(login) {
                    mProgressDialog.show();
                    mLoginResponseCache = new HttpResponseCache(getActivity(), APIManager.login(mUserName.getText().toString(), mPassword.getText().toString(), APIManager.userIMEI), "");
                    mLoginResponseCache.method = HttpResponseCache.POST_METHOD;

                    mLoginResponseCache.addParameter(new Pair<String, String>("username", mUserName.getText().toString()));
                    mLoginResponseCache.addParameter(new Pair<String, String>("password", mPassword.getText().toString()));
                    mLoginResponseCache.addParameter(new Pair<String, String>("imei", APIManager.userIMEI));

                    mLoginResponseCache.setWillAddToCache(false);
                    mLoginResponseCache.setObjectListener(new HttpResponseCache.OnDataLoaderListener() {
                        @Override
                        public void onDataLoadComplete(JSONObject jsonObject) {
                            try {
                                JSONArray loginArray = jsonObject.getJSONArray("data");
                                JSONObject loginObject = loginArray.getJSONObject(0);
                                String status = loginObject.getString("success");
                                String userID = loginObject.getString("userid");
                                String userType = loginObject.getString("usertype");
                                String flagged = loginObject.getString("flagged");

                                if(status.equals("true")) {
                                    // login success

                                    APIManager.userID = userID;
                                    APIManager.userType = userType;
                                    APIManager.userCanScan = flagged;

                                    // save user data
                                    mLocalAppData.setValueForKey("login_oneberry", "userID", userID);
                                    mLocalAppData.setValueForKey("login_oneberry", "userType", userType);
                                    mLocalAppData.setValueForKey("login_oneberry", "userCanScan", flagged);

                                    mProgressDialog.dismiss();
                                    Intent intent = new Intent(getActivity(), HomeActivity.class);
                                    getActivity().startActivity(intent);
                                    getActivity().finish();
                                }else{
                                    mProgressDialog.dismiss();

                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Login").setMessage("Login failed. Please try again")
                                            .setPositiveButton("OK", null);
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                    dialog.setCancelable(false);
                                }
                            }catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onDataLoadFailed() {

                        }

                        @Override
                        public void onCanceled() {
                            mLoginResponseCache.cancel(true);
                            mProgressDialog.dismiss();

                            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Login").setMessage("Login failed. Connection Timeout")
                                    .setPositiveButton("OK", null);
                            AlertDialog dialog = builder.create();
                            dialog.show();
                            dialog.setCancelable(false);
                        }
                    });
                    mLoginResponseCache.execute();
                }
            }
        });

        return view;
    }
}
