package com.oneberry.oneberryapp.controller.fragments.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.groupm.labs.HttpResponseCache;
import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.adapter.SiteListRecyclerAdapter;
import com.oneberry.oneberryapp.data.APIManager;
import com.oneberry.oneberryapp.model.Site;
import com.oneberry.oneberryapp.model.managers.SitesManager;
import com.oneberry.oneberryapp.utils.MyTypeface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/29/16.
 */
public class SiteListFragment extends Fragment {

    TextView mContentInfoTextView;
    RecyclerView mSiteListRecyclerView;
    LinearLayoutManager mLinearLayoutManager;
    ProgressDialog mProgressDialog;

    // data
    private SitesManager mSitesManager;
    private SiteListRecyclerAdapter mSiteListRecyclerAdapter;
    private HttpResponseCache mDataLoader;
    private Spinner mFilterSpinner;
    private String mUrlString;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSitesManager = SitesManager.getInstance(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.site_list_fragment, container, false);

        // progress dialog
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading Active Sites");

        // textview
        mContentInfoTextView = (TextView)view.findViewById(R.id.info_textView);
        mContentInfoTextView.setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));
        mContentInfoTextView.setText("Loading Sites..");

        // set recycler view
        mSiteListRecyclerView = (RecyclerView)view.findViewById(R.id.site_list);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mSiteListRecyclerView.setLayoutManager(mLinearLayoutManager);

        // set adapter
        mSiteListRecyclerAdapter = new SiteListRecyclerAdapter(mSitesManager.getSiteList());
        mSiteListRecyclerView.setAdapter(mSiteListRecyclerAdapter);

        // set filter
        // set spinner content
        mFilterSpinner = (Spinner)view.findViewById(R.id.filter_spinner);
        ArrayList<String> filterList = new ArrayList<>();
        filterList.add("Active");
        filterList.add("Archived");
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(), filterList);
        mFilterSpinner.setAdapter(adapter);

        mFilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mContentInfoTextView.setText("Loading Sites");

                if(position == 0) {
                    mSitesManager.removeAllData();
                    mSiteListRecyclerAdapter.notifyDataSetChanged();

                    mUrlString = APIManager.GET_SITE_LIST;
                    loadSiteData();
                    mProgressDialog.setMessage("Loading Active Sites");
                    mProgressDialog.show();
                }else{
                    // remove data
                    mSitesManager.removeAllData();
                    mSiteListRecyclerAdapter.notifyDataSetChanged();

                    mUrlString = APIManager.GET_ARCHIVED_SITE;
                    loadSiteData();
                    mProgressDialog.setMessage("Loading Archived Sites");
                    mProgressDialog.show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    private void loadSiteData() {
        // set data loader
        mDataLoader = new HttpResponseCache(getActivity(), mUrlString, "");
        mDataLoader.setWillAddToCache(false);
        mDataLoader.setObjectListener(new HttpResponseCache.OnDataLoaderListener() {
            @Override
            public void onDataLoadComplete(JSONObject jsonObject) {
                try {
                    JSONArray siteArray = jsonObject.getJSONArray("data");
                    int jsonLength = siteArray.length();


                    mContentInfoTextView.setText("Number of Sites: " + jsonLength);

                    for(int i = 0; i < jsonLength; i++) {
                        JSONObject siteObject = siteArray.getJSONObject(i);
                        Site site = new Site();
                        site.setAccountID(siteObject.getString("accountid"));
                        site.setsiteID(siteObject.getString("siteid"));
                        site.setSiteName(siteObject.getString("siteName"));
                        site.setSiteType(siteObject.getString("siteType"));
                        site.setAddress1(siteObject.getString("address1"));
                        site.setAddress2(siteObject.getString("address2"));
                        site.setCountry(siteObject.getString("country"));
                        site.setZipCode(siteObject.getString("zipCode"));
                        site.setOthers(siteObject.getString("others"));
                        site.setDateAdded(siteObject.getString("dateAdded"));
                        site.setSiteIndex(i);
                        mSitesManager.addSites(site);
                    }

                    mSiteListRecyclerAdapter.notifyDataSetChanged();
                    mProgressDialog.dismiss();
                }catch(JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDataLoadFailed() {

            }

            @Override
            public void onCanceled() {

            }
        });
        mDataLoader.execute();
    }

    public void searchSite(String searchKey) {
        mSitesManager.removeAllData();
        mSiteListRecyclerAdapter.notifyDataSetChanged();
        mProgressDialog.setMessage("Searching: " + searchKey);
        mProgressDialog.show();
        if(mFilterSpinner.getSelectedItemPosition() == 0) {
            mUrlString = APIManager.searchSite(searchKey);
        }else{
            mUrlString = APIManager.searchArchivedSite(searchKey);
        }
        loadSiteData();
    }

    private class CustomSpinnerAdapter extends BaseAdapter {
        ArrayList<String> stringArrayList = new ArrayList<>();
        Context context;

        public CustomSpinnerAdapter(Context context, ArrayList<String> arrayList) {
            this.context = context;
            this.stringArrayList = arrayList;
        }

        @Override
        public int getCount() {
            return stringArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if(view == null) {
                // we'll create a custom view
                view = LayoutInflater.from(context).inflate(R.layout.spinner_basic_layout, parent, false);

                TextView spinnerTextView = (TextView)view.findViewById(R.id.spinner_textview);
                spinnerTextView.setText(stringArrayList.get(position));
                spinnerTextView.setTypeface(MyTypeface.get(context, "clan-book-webfont"));
            }

            return view;
        }
    }
}
