package com.oneberry.oneberryapp.controller.list.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.model.AccountContact;
import com.oneberry.oneberryapp.utils.MyTypeface;

/**
 * Created by zechariah.jabines on 3/29/16.
 */
public class AccountContactListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    //UIs
    EditText mFirstNameEditText, mLastNameEditText, mEmailEditText, mMobileEditText;
    int contactIndex;

    public AccountContactListViewHolder(View itemView) {
        super(itemView);

        // bind ui
        mFirstNameEditText = (EditText)itemView.findViewById(R.id.firstname);
        mLastNameEditText = (EditText)itemView.findViewById(R.id.lastname);
        mEmailEditText = (EditText)itemView.findViewById(R.id.email);
        mMobileEditText = (EditText)itemView.findViewById(R.id.mobile);

        mFirstNameEditText.setTypeface(MyTypeface.get(itemView.getContext(), "clan-book-webfont"));
        mLastNameEditText.setTypeface(MyTypeface.get(itemView.getContext(), "clan-book-webfont"));
        mEmailEditText.setTypeface(MyTypeface.get(itemView.getContext(), "clan-book-webfont"));
        mMobileEditText.setTypeface(MyTypeface.get(itemView.getContext(), "clan-book-webfont"));

        itemView.setOnClickListener(this);
    }

    public void bindData(AccountContact accountContact) {
        mFirstNameEditText.setText(accountContact.getFirstName());
        mLastNameEditText.setText(accountContact.getLastName());
        mEmailEditText.setText(accountContact.getEmail());
        mMobileEditText.setText(accountContact.getMobile());

        contactIndex = accountContact.getContactIndex();
    }

    @Override
    public void onClick(View v) {
        // TODO: 3/29/16
    }
}
