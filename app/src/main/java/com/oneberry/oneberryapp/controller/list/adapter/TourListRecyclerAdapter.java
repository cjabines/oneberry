package com.oneberry.oneberryapp.controller.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.viewholder.TourListRecyclerViewHolder;
import com.oneberry.oneberryapp.model.Tour;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/29/16.
 */
public class TourListRecyclerAdapter extends RecyclerView.Adapter<TourListRecyclerViewHolder> {

    ArrayList<Tour> mTours = new ArrayList<>();

    public TourListRecyclerAdapter(ArrayList<Tour> arrayList) {
        super();
        mTours = arrayList;
    }

    @Override
    public TourListRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tour_list_item_layout, parent, false);
        return new TourListRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TourListRecyclerViewHolder holder, int position) {
        holder.bindData(mTours.get(position));
    }

    @Override
    public int getItemCount() {
        return mTours.size();
    }
}
