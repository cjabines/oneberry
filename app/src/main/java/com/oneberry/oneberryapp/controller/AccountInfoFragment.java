package com.oneberry.oneberryapp.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.adapter.AccountContactListRecyclerAdapter;
import com.oneberry.oneberryapp.controller.list.viewholder.AccountListRecyclerViewHolder;
import com.oneberry.oneberryapp.model.Account;
import com.oneberry.oneberryapp.model.AccountContact;
import com.oneberry.oneberryapp.model.managers.AccountsManager;
import com.oneberry.oneberryapp.utils.MyTypeface;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/28/16.
 */
public class AccountInfoFragment extends Fragment {

    TextView mBasicInfoTextView, mContactInfoTextView, mToolbarTextView;
    EditText mAccountIDEditText, mAccountNameEditText, mAccountAddressEditText, mAccountZipCodeEditText, mAccountCountryEditText;
    int mAccountIndex;
    AccountsManager mAccountsManager;
    ArrayList<AccountContact> mAccountContacts;

    RecyclerView mContactListRecyclerView;
    LinearLayoutManager mLinearLayoutManager;
    AccountContactListRecyclerAdapter mAccountContactListRecyclerAdapter;
    JSONArray mContactsArray;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mAccountIndex = getActivity().getIntent().getIntExtra(AccountListRecyclerViewHolder.ACCOUNT_INDEX_EXTRA, 0);
        mAccountsManager = AccountsManager.getInstance(getActivity());
        mAccountContacts = mAccountsManager.getAccountList().get(mAccountIndex).getAccountContacts();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.account_info_fragment, container, false);

        // bind ui
        Toolbar toolbar = (Toolbar)view.findViewById(R.id.toolbar);
        mToolbarTextView = (TextView)view.findViewById(R.id.toolbar_textview);
        mToolbarTextView.setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));

        // bind ui
        mBasicInfoTextView = (TextView)view.findViewById(R.id.basicinfo);
        mContactInfoTextView = (TextView)view.findViewById(R.id.contactinfo);
        mAccountIDEditText = (EditText)view.findViewById(R.id.accountid);
        mAccountNameEditText = (EditText)view.findViewById(R.id.accountname);
        mAccountAddressEditText = (EditText)view.findViewById(R.id.accountaddress);
        mAccountZipCodeEditText = (EditText)view.findViewById(R.id.accountzipcode);
        mAccountCountryEditText = (EditText)view.findViewById(R.id.accountcountry);
        mContactListRecyclerView = (RecyclerView)view.findViewById(R.id.contactlist);

        // set font
        mBasicInfoTextView.setTypeface(MyTypeface.get(getActivity(), "clan-med-webfont"));
        mContactInfoTextView.setTypeface(MyTypeface.get(getActivity(), "clan-med-webfont"));

        EditText[] editTexts = {mAccountIDEditText, mAccountNameEditText, mAccountAddressEditText, mAccountZipCodeEditText, mAccountCountryEditText};
        for(int i = 0; i < editTexts.length; i++) {
            editTexts[i].setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));
        }

        // add data
        Account account = mAccountsManager.getAccountList().get(mAccountIndex);
        mAccountIDEditText.setText(account.getAccountID());
        mAccountNameEditText.setText(account.getCompanyName());
        mAccountAddressEditText.setText(account.getAddress1());
        mAccountZipCodeEditText.setText(account.getZipCode());
        mAccountCountryEditText.setText(account.getCountry());

        // set contact
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mContactListRecyclerView.setLayoutManager(mLinearLayoutManager);

        mAccountContactListRecyclerAdapter = new AccountContactListRecyclerAdapter(mAccountContacts);
        mContactListRecyclerView.setAdapter(mAccountContactListRecyclerAdapter);

        // set up actionbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return true;
    }
}
