package com.oneberry.oneberryapp.controller.fragments.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.groupm.labs.HttpResponseCache;
import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.adapter.AccountListRecyclerAdapter;
import com.oneberry.oneberryapp.data.APIManager;
import com.oneberry.oneberryapp.model.Account;
import com.oneberry.oneberryapp.model.managers.AccountsManager;
import com.oneberry.oneberryapp.utils.MyTypeface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/21/16.
 */
public class AccountListFragment extends Fragment {

    TextView mContentInfoTextView;
    RecyclerView mAccoutListRecyclerView;
    LinearLayoutManager mLinearLayoutManager;

    // data
    private AccountsManager mAccountManager;
    private AccountListRecyclerAdapter mAccountListRecyclerAdapter;
    private HttpResponseCache mDataLoader;
    private Spinner mFilterSpinner;
    private String mUrlString;

    // progress
    private ProgressDialog mProgressDialog;


    private static final String TAG = "AccountListFragment";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAccountManager = AccountsManager.getInstance(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.account_list_fragment, container, false);
        // progress dialog
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading Active Accounts");

        // textview
        mContentInfoTextView = (TextView)view.findViewById(R.id.info_textView);
        mContentInfoTextView.setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));
        mContentInfoTextView.setText("Loading Accounts..");

        // set recycler view
        mAccoutListRecyclerView = (RecyclerView)view.findViewById(R.id.account_list);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mAccoutListRecyclerView.setLayoutManager(mLinearLayoutManager);

        // set adapter
        mAccountListRecyclerAdapter = new AccountListRecyclerAdapter(mAccountManager.getAccountList());
        mAccoutListRecyclerView.setAdapter(mAccountListRecyclerAdapter);

        // set filter
        // set spinner content
        mFilterSpinner = (Spinner)view.findViewById(R.id.filter_spinner);
        ArrayList<String> filterList = new ArrayList<>();
        filterList.add("Active");
        filterList.add("Archived");
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(), filterList);
        mFilterSpinner.setAdapter(adapter);

        mFilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mContentInfoTextView.setText("Loading Accounts");

                if(position == 0) {
                    mAccountManager.removeAllData();
                    mAccountListRecyclerAdapter.notifyDataSetChanged();

                    mUrlString = APIManager.GET_ACCOUNT_LIST;
                    loadAccountData();
                    mProgressDialog.setMessage("Loading Active Accounts");
                    mProgressDialog.show();
                }else{
                    // remove data
                    mAccountManager.removeAllData();
                    mAccountListRecyclerAdapter.notifyDataSetChanged();

                    mUrlString = APIManager.GET_ARCHIVED_ACCOUNT;
                    loadAccountData();
                    mProgressDialog.setMessage("Loading Archived Accounts");
                    mProgressDialog.show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

    private void loadAccountData() {
        // set data loader
        mDataLoader = new HttpResponseCache(getActivity(), mUrlString, "");
        mDataLoader.setWillAddToCache(false);
        mDataLoader.setObjectListener(new HttpResponseCache.OnDataLoaderListener() {
            @Override
            public void onDataLoadComplete(JSONObject jsonObject) {
                try {
                    JSONArray accountArray = jsonObject.getJSONArray("data");
                    int jsonLength = accountArray.length();


                    mContentInfoTextView.setText("Number of Accounts: " + jsonLength);

                    for(int i = 0; i < jsonLength; i++) {
                        JSONObject accountObject = accountArray.getJSONObject(i);
                        Account account = new Account();
                        account.setCompanyName(accountObject.getString("companyname"));
                        account.setAccountID(accountObject.getString("accountid"));
                        account.setAddress1(accountObject.getString("address"));
                        account.setCountry(accountObject.getString("country"));
                        account.setZipCode(accountObject.getString("zipcode"));
                        account.setAccountContacts(accountObject.getJSONObject("contacts").getJSONArray("contactinfo"));

                        mAccountManager.addAccount(account);
                    }
                    mAccountListRecyclerAdapter.notifyDataSetChanged();
                    mProgressDialog.dismiss();
                }catch(JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDataLoadFailed() {

            }

            @Override
            public void onCanceled() {

            }
        });
        mDataLoader.execute();
    }

    public void searchAccount(String searchKey) {
        mAccountManager.removeAllData();
        mAccountListRecyclerAdapter.notifyDataSetChanged();
        mProgressDialog.setMessage("Searching: " + searchKey);
        mProgressDialog.show();
        if(mFilterSpinner.getSelectedItemPosition() == 0) {
            mUrlString = APIManager.searchAccount(searchKey);
        }else{
            mUrlString = APIManager.searchArchivedAccount(searchKey);
        }
        loadAccountData();
    }

    private class CustomSpinnerAdapter extends BaseAdapter {
        ArrayList<String> stringArrayList = new ArrayList<>();
        Context context;

        public CustomSpinnerAdapter(Context context, ArrayList<String> arrayList) {
            this.context = context;
            this.stringArrayList = arrayList;
        }

        @Override
        public int getCount() {
            return stringArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if(view == null) {
                // we'll create a custom view
                view = LayoutInflater.from(context).inflate(R.layout.spinner_basic_layout, parent, false);

                TextView spinnerTextView = (TextView)view.findViewById(R.id.spinner_textview);
                spinnerTextView.setText(stringArrayList.get(position));
                spinnerTextView.setTypeface(MyTypeface.get(context, "clan-book-webfont"));
            }

            return view;
        }
    }
}
