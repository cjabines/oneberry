package com.oneberry.oneberryapp.controller.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.viewholder.ToursCheckpointListRecyclerViewHolder;
import com.oneberry.oneberryapp.model.Checkpoint;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/29/16.
 */
public class ToursCheckpointListRecyclerAdapter extends RecyclerView.Adapter<ToursCheckpointListRecyclerViewHolder> {

    ArrayList<Checkpoint> mTourCheckpoint = new ArrayList<>();

    public ToursCheckpointListRecyclerAdapter(ArrayList<Checkpoint> arrayList) {
        super();
        mTourCheckpoint = arrayList;
    }

    @Override
    public ToursCheckpointListRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.tour_checkpoint_list_layout, parent, false);
        return new ToursCheckpointListRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ToursCheckpointListRecyclerViewHolder holder, int position) {
        holder.bindData(mTourCheckpoint.get(position));
    }

    @Override
    public int getItemCount() {
        return mTourCheckpoint.size();
    }
}

