package com.oneberry.oneberryapp.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.oneberry.oneberryapp.utils.SingleFragmentActivity;

/**
 * Created by zechariah.jabines on 3/28/16.
 */
public class AccountInfoActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new AccountInfoFragment();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
