package com.oneberry.oneberryapp.controller;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.oneberry.oneberryapp.utils.SingleFragmentActivity;

/**
 * Created by zechariah.jabines on 3/30/16.
 */
public class CheckpointInfoActivity extends SingleFragmentActivity {

    CheckpointInfoFragment mCheckpointInfoFragment;

    // NFC
    protected NfcAdapter nfcAdapter;
    protected PendingIntent nfcPendingIntent;

    @Override
    protected Fragment createFragment() {
        // initialize NFC
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        mCheckpointInfoFragment = new CheckpointInfoFragment();
        return mCheckpointInfoFragment;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            mCheckpointInfoFragment.nfcTagDetected();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(nfcAdapter != null) {
            disableForegroundMode();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(nfcAdapter != null) {
            enableForegroundMode();
        }
    }

    public void enableForegroundMode() {
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED); // filter for all
        IntentFilter[] writeTagFilters = new IntentFilter[] {tagDetected};
        nfcAdapter.enableForegroundDispatch(this, nfcPendingIntent, writeTagFilters, null);
    }

    public void disableForegroundMode() {
        nfcAdapter.disableForegroundDispatch(this);
    }
}
