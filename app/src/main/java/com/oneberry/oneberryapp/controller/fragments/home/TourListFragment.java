package com.oneberry.oneberryapp.controller.fragments.home;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.groupm.labs.HttpResponseCache;
import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.data.APIManager;
import com.oneberry.oneberryapp.model.CheckpointResponse;
import com.oneberry.oneberryapp.model.managers.CheckpointIDManager;
import com.oneberry.oneberryapp.services.TimerServices;
import com.oneberry.oneberryapp.utils.MyTypeface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/21/16.
 */
public class TourListFragment extends Fragment {

    TextView mContentInfoTextView, mTextViewInfoMessage;
    private static final String TAG = "TourListFragment";
    private boolean isScanning;
    //private ProgressDialog //mProgressDialog;
    private HttpResponseCache mResponseCache;

    private String tagSample = "10001";
    private int tagSimulator = 0;

    public static final String EXTRA_TIME = "EXTRA_TIME";
    private boolean timerDialog = false;
    private static ArrayList<String> tagID = new ArrayList<>();

    private Intent timerService;

    private final int DAY_MILL = 86400000;
    private final int HOUR_MILL = 3600000;
    private final int MINUTE_MILL = 60000;

    private CheckpointIDManager checkpointIDManager;


    //TODO: remove this later
    Button one, two;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        timerDialog = getActivity().getIntent().getBooleanExtra(TimerServices.EXTRA_DIALOG, false);
        if(timerDialog) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                    .setTitle("Mobile Guard Tour")
                    .setMessage("Session Expired. Please restart the tour")
                    .setPositiveButton("OK", null);

            AlertDialog dialog = builder.create();
            dialog.setCancelable(false);
            dialog.show();

            MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(), R.raw.ring);
            mediaPlayer.start();

            // turn off the service
            getActivity().stopService(timerService);
        }

        checkpointIDManager = CheckpointIDManager.get(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tour_list_fragment, container, false);

        //mProgressDialog = new ProgressDialog(getActivity());
        //mProgressDialog.setCancelable(false);

        // textview
        mContentInfoTextView = (TextView)view.findViewById(R.id.info_textView);
        mContentInfoTextView.setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));


        // TODO: remove this later
        one = (Button)view.findViewById(R.id.one);
        two = (Button)view.findViewById(R.id.two);

        one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nfcTagDetected("10101010");
            }
        });

        two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nfcTagDetected("20202020");
            }
        });

        String info, message = "";
        /*if(APIManager.userCanScan.equals("true")) {
            info = "Scan Ready";
            message = "Tap the NFC to start scanning";
        }else{
            info = "Scan Blocked";
            message = "Scan Blocked. Unauthorized unit detected. Please contact your administrator";
        }*/
        info = "Scan Ready";
        message = "Tap the NFC to start scanning";

        mContentInfoTextView.setText(info);

        mTextViewInfoMessage = (TextView)view.findViewById(R.id.info_message);
        mTextViewInfoMessage.setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));
        mTextViewInfoMessage.setText(message);
        return view;
    }

    // NFC DETECT
    public void nfcTagDetected(final String serialNumber) {
        //mProgressDialog.setMessage("Scanning Checkpoint \nSerial Number: "+serialNumber);

        //TODO: remove this later
        //APIManager.userIMEI = "863078020009447";

        if(!isScanning) {
            isScanning = true;
            //mProgressDialog.show();
            mResponseCache = new HttpResponseCache(getActivity(), APIManager.reportCheckpoint(APIManager.userID, serialNumber, APIManager.userIMEI), "");
            mResponseCache.method = HttpResponseCache.GET_METHOD;

            mResponseCache.addParameter(new Pair<String, String>("userID", APIManager.userID));
            mResponseCache.addParameter(new Pair<String, String>("tagID", serialNumber));
            mResponseCache.addParameter(new Pair<String, String>("imei", APIManager.userIMEI));

            mResponseCache.setWillAddToCache(false);
            mResponseCache.setObjectListener(new HttpResponseCache.OnDataLoaderListener() {
                @Override
                public void onDataLoadComplete(JSONObject jsonObject) {
                    //mProgressDialog.dismiss();
                    Log.d("onDataLoadComplete", "onDataLoadComplete: " + jsonObject.toString());
                    tagSimulator = 1;
                    try {
                        JSONArray checkpointArray = jsonObject.getJSONArray("data");
                        JSONObject object = checkpointArray.getJSONObject(0);

                        CheckpointResponse response = new CheckpointResponse();
                        response.setIsSuccess(object.getString("success"));
                        response.setIsNewCheckpoint(object.getString("newCheckpoint"));
                        response.setLocation(object.getString("location"));
                        response.setCheckpointID(object.getString("checkpointID"));
                        response.setReply(object.getString("reply"));
                        response.setFinished(object.getString("finished"));
                        response.setDuration(object.getString("duration"));

                        if(response.getIsSuccess().equals("'true'")) {

                            // if success we'll remove the tagID in list
                            tagID.remove(serialNumber);

                            // if old tag - we scanned successfully
                            if(response.getIsNewCheckpoint().equals("'false'")) {
                                if(response.getFinished().equals("yes")) { // tour done
                                    // play a sound if complete
                                    MediaPlayer mediaPlayer = MediaPlayer.create(getActivity(), R.raw.ring);
                                    mediaPlayer.start();

                                    // alert
                                    //mProgressDialog.dismiss();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Checkpoint").setMessage("Tour Complete!")
                                            .setPositiveButton("OK", null);
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                    dialog.setCancelable(false);

                                    if(response.getDuration().equals("9000")) {
                                        getActivity().stopService(timerService);
                                        timerService = null;
                                    }

                                    checkpointIDManager.clearIDs();

                                }else{
                                    // alert
                                    //mProgressDialog.dismiss();
                                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Checkpoint").setMessage("Scan Complete! Please proceed to the next checkpoint")
                                            .setPositiveButton("OK", null);
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                    dialog.setCancelable(false);

                                    // start timer service if there's duration
                                    timerService = new Intent(getActivity(), TimerServices.class);
                                    if(!response.getDuration().equals("0")) {

                                        // get the duration based on Clifton's format
                                        String[] duration = response.getDuration().split(" ");
                                        int day = 0;
                                        int hour = getDurationFromString(duration[1]);
                                        int minute = getDurationFromString(duration[2]);
                                        int finalTime = day + hour + minute;

                                        timerService.putExtra(EXTRA_TIME, finalTime);
                                        getActivity().startService(timerService);
                                    }

                                    checkpointIDManager.addIDs(response.getCheckpointID());
                                }
                            }else{ // if new tag is discovered
                                //mProgressDialog.dismiss();
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Checkpoint").setMessage("New Checkpoint Created: \n\nCheckpoint ID: "+response.getCheckpointID())
                                        .setPositiveButton("OK", null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                dialog.setCancelable(false);
                            }
                        }else{
                            //mProgressDialog.dismiss();

                            if(response.getReply().equals("New Device Detected")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Checkpoint").setMessage("Scan Blocked. Unauthorized unit detected. Please contact your administrator")
                                        .setPositiveButton("OK", null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                dialog.setCancelable(false);
                            } else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Checkpoint").setMessage("Failed to scan checkpoint." + "\n" + response.getReply())
                                        .setPositiveButton("OK", null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                dialog.setCancelable(false);
                            }
                        }

                        isScanning = false;
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                    isScanning = false;
                }

                @Override
                public void onDataLoadFailed() {
                    //mProgressDialog.dismiss();
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Checkpoint").setMessage("Failed to scan checkpoint. Failed to write to database")
                            .setPositiveButton("OK", null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    dialog.setCancelable(false);

                    isScanning = false;
                }

                @Override
                public void onCanceled() {
                    mResponseCache.cancel(true);
                    //mProgressDialog.dismiss();

                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Login").setMessage("Scan failed. Connection Timeout")
                            .setPositiveButton("OK", null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                    dialog.setCancelable(false);

                    isScanning = false;
                }
            });
            mResponseCache.execute();
        }
    }

    private int getDurationFromString(String durationString) {
        int value = 0;
        String type = durationString.substring(durationString.length() - 1);
        String typeValue = durationString.substring(0, durationString.length() - 1);
        Log.d(TAG, "getDurationFromString: " + typeValue);
        switch (type) {
            case "d": // days
                value = Integer.parseInt(typeValue) * DAY_MILL;
                break;

            case "h": // hours
                value = Integer.parseInt(typeValue) * HOUR_MILL;
                break;

            case "m": // minutes
                value = Integer.parseInt(typeValue) * MINUTE_MILL;
                break;
        }
        return value;
    }
}
