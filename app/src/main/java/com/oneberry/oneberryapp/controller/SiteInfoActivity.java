package com.oneberry.oneberryapp.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.oneberry.oneberryapp.utils.SingleFragmentActivity;

/**
 * Created by zechariah.jabines on 3/29/16.
 */
public class SiteInfoActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new SiteInfoFragment();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
