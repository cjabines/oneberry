package com.oneberry.oneberryapp.controller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.groupm.labs.HttpResponseCache;
import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.adapter.ToursCheckpointListRecyclerAdapter;
import com.oneberry.oneberryapp.controller.list.viewholder.TourListRecyclerViewHolder;
import com.oneberry.oneberryapp.data.APIManager;
import com.oneberry.oneberryapp.model.Tour;
import com.oneberry.oneberryapp.model.managers.CheckpointsManager;
import com.oneberry.oneberryapp.model.managers.ToursManager;
import com.oneberry.oneberryapp.utils.MyTypeface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zechariah.jabines on 3/31/16.
 */
public class TourCheckpointsFragment extends Fragment {

    // ui
    TextView mTourInfoTextView, mTourToolbarTextView, mTourCheckpointTextView;
    EditText mTourIDEditText, mTourNameEditText, mTourTypeEditText, mTourDaysEditText;
    RecyclerView mCheckpointsRecyclerView;
    int mTourIndex;
    ToursManager mToursManager;
    CheckpointsManager mCheckpointsManager;

    // list data
    ToursCheckpointListRecyclerAdapter mToursCheckpointListRecyclerAdapter;
    Tour mTour;

    // simulate tagging
    ProgressDialog mProgressDialog;
    HttpResponseCache mResponseCache;

    // flag for scanning
    boolean isScanning = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTourIndex = getActivity().getIntent().getIntExtra(TourListRecyclerViewHolder.EXTRA_TOUR_INDEX, 0);
        mToursManager = ToursManager.getInstance(getActivity());
        mCheckpointsManager = CheckpointsManager.getInstance(getActivity());
        setHasOptionsMenu(true);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Scanning new Checkpoint... Please wait");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tour_checkpoints_info_fragment, container, false);

        // bind ui
        Toolbar toolbar = (Toolbar)v.findViewById(R.id.toolbar);
        mTourInfoTextView = (TextView)v.findViewById(R.id.tourinformation);
        mTourToolbarTextView = (TextView)v.findViewById(R.id.toolbar_textview);
        mTourCheckpointTextView = (TextView)v.findViewById(R.id.tourcheckpoints);

        mTourIDEditText = (EditText)v.findViewById(R.id.tourcheckpointid);
        mTourNameEditText = (EditText)v.findViewById(R.id.tourcheckpointname);
        mTourTypeEditText = (EditText)v.findViewById(R.id.tourcheckpointtype);
        mTourDaysEditText = (EditText)v.findViewById(R.id.tourcheckpointdays);

        mCheckpointsRecyclerView = (RecyclerView)v.findViewById(R.id.tourcheckpoint_list);

        // set toolbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // set fonts
        EditText[] editTexts = {mTourIDEditText, mTourNameEditText, mTourTypeEditText, mTourDaysEditText};
        for(int i = 0; i < editTexts.length; i++) {
            editTexts[i].setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));
        }
        mTourInfoTextView.setTypeface(MyTypeface.get(getActivity(), "clan-med-webfont"));
        mTourToolbarTextView.setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));
        mTourCheckpointTextView.setTypeface(MyTypeface.get(getActivity(), "clan-med-webfont"));

        // bind basic data
        mTour = mToursManager.getTourList().get(mTourIndex);
        mTourIDEditText.setText(mTour.getTourID());
        mTourNameEditText.setText(mTour.getGuardTourName());
        mTourTypeEditText.setText(mTour.getTourType());
        mTourDaysEditText.setText(mTour.getDays());

        // set up recycler view
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mCheckpointsRecyclerView.setLayoutManager(linearLayoutManager);

        mCheckpointsManager.addCheckpointList(mToursManager.getTourList().get(mTourIndex).getTourCheckpointList());
        mToursCheckpointListRecyclerAdapter = new ToursCheckpointListRecyclerAdapter(mCheckpointsManager.getCheckpointList());
        mCheckpointsRecyclerView.setAdapter(mToursCheckpointListRecyclerAdapter);

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        mToursCheckpointListRecyclerAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return true;
    }

    public void nfcTagDetected(String id) {
        if(!isScanning) {
            isScanning = true;
            mProgressDialog.show();
            mResponseCache = new HttpResponseCache(getActivity(), APIManager.reportCheckpoint(APIManager.userID, mTour.getTourID(), APIManager.userIMEI), "");
            mResponseCache.setWillAddToCache(false);
            mResponseCache.setObjectListener(new HttpResponseCache.OnDataLoaderListener() {
                @Override
                public void onDataLoadComplete(JSONObject jsonObject) {
                    mProgressDialog.dismiss();
                    Log.d("onDataLoadComplete", "onDataLoadComplete: " + jsonObject.toString());

                    try {
                        JSONArray checkpointArray = jsonObject.getJSONArray("data");
                        JSONObject checkpointObject = checkpointArray.getJSONObject(0);
                        String checkID = checkpointObject.getString("checkpointid");
                        String success = checkpointObject.getString("success");
                        if(success.equals("true")) {
                            if(!checkID.equals("")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Checkpoint").setMessage("New checkpoint added")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {

                                            }
                                        });
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                dialog.setCancelable(false);
                            }else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Checkpoint").setMessage("Failed to add new checkpoint. Please try again")
                                        .setPositiveButton("OK", null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                dialog.setCancelable(false);
                            }
                        }
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                    isScanning = false;
                }

                @Override
                public void onDataLoadFailed() {

                }

                @Override
                public void onCanceled() {

                }
            });
            //mResponseCache.execute();
        }
    }
}
