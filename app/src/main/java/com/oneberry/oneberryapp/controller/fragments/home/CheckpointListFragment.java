package com.oneberry.oneberryapp.controller.fragments.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.groupm.labs.HttpResponseCache;
import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.adapter.CheckpointListRecyclerAdapter;
import com.oneberry.oneberryapp.data.APIManager;
import com.oneberry.oneberryapp.model.Checkpoint;
import com.oneberry.oneberryapp.model.managers.CheckpointsManager;
import com.oneberry.oneberryapp.utils.MyTypeface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/29/16.
 */
public class CheckpointListFragment extends Fragment {

    TextView mContentInfoTextView;
    RecyclerView mCheckpointListRecyclerView;
    LinearLayoutManager mLinearLayoutManager;

    // data
    private CheckpointsManager mCheckpointsManager;
    private CheckpointListRecyclerAdapter mCheckpointListRecyclerAdapter;
    private HttpResponseCache mDataLoader;
    private Spinner mFilterSpinner;
    private String mUrlString;

    // progress
    private ProgressDialog mProgressDialog;


    private static final String TAG = "CheckpointListFragment";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mCheckpointsManager = CheckpointsManager.getInstance(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.checkpoint_list_fragment, container, false);

        // progress dialog
        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading Active Checkpoints");

        // textview
        mContentInfoTextView = (TextView)view.findViewById(R.id.info_textView);
        mContentInfoTextView.setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));
        mContentInfoTextView.setText("Loading Checkpoints..");

        // set recycler view
        mCheckpointListRecyclerView = (RecyclerView)view.findViewById(R.id.check_list);
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mCheckpointListRecyclerView.setLayoutManager(mLinearLayoutManager);

        // set adapter
        mCheckpointListRecyclerAdapter = new CheckpointListRecyclerAdapter(mCheckpointsManager.getCheckpointList());
        mCheckpointListRecyclerView.setAdapter(mCheckpointListRecyclerAdapter);

        // set filter
        // set spinner content
        mFilterSpinner = (Spinner)view.findViewById(R.id.filter_spinner);
        ArrayList<String> filterList = new ArrayList<>();
        filterList.add("Active");
        filterList.add("Archived");
        CustomSpinnerAdapter adapter = new CustomSpinnerAdapter(getActivity(), filterList);
        mFilterSpinner.setAdapter(adapter);

        mFilterSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mContentInfoTextView.setText("Loading Checkpoints");

                if(position == 0) {
                    mCheckpointsManager.removeAllData();
                    mCheckpointListRecyclerAdapter.notifyDataSetChanged();

                    mUrlString = APIManager.GET_CHECKPOINT_LIST;
                    loadCheckpointData();
                    mProgressDialog.setMessage("Loading Active Checkpoints");
                    mProgressDialog.show();
                }else{
                    // remove data
                    mCheckpointsManager.removeAllData();
                    mCheckpointListRecyclerAdapter.notifyDataSetChanged();

                    mUrlString = APIManager.GET_ARCHIVED_CHECKPOINT;
                    loadCheckpointData();
                    mProgressDialog.setMessage("Loading Archived Checkpoints");
                    mProgressDialog.show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        return view;
    }

    private void loadCheckpointData() {
        // set data loader
        mDataLoader = new HttpResponseCache(getActivity(), mUrlString, "");
        mDataLoader.setWillAddToCache(false);
        mDataLoader.setObjectListener(new HttpResponseCache.OnDataLoaderListener() {
            @Override
            public void onDataLoadComplete(JSONObject jsonObject) {
                try {
                    JSONArray checkpointArray = jsonObject.getJSONArray("data");
                    int jsonLength = checkpointArray.length();


                    mContentInfoTextView.setText("Number of Checkpoints: " + jsonLength);

                    for(int i = 0; i < jsonLength; i++) {
                        JSONObject checkpointObject = checkpointArray.getJSONObject(i);
                        Checkpoint checkpoint = new Checkpoint();
                        checkpoint.setBranch(checkpointObject.getString("branch"));
                        checkpoint.setCheckpointName(checkpointObject.getString("checkpointName"));
                        checkpoint.setCheckpointType(checkpointObject.getString("checkpointType"));
                        checkpoint.setTagID(checkpointObject.getString("tagid"));
                        checkpoint.setLocation(checkpointObject.getString("location"));
                        checkpoint.setCoordinates(checkpointObject.getString("coordinates"));
                        checkpoint.setNotes(checkpointObject.getString("notes"));
                        checkpoint.setCheckpointIndex(i);
                        mCheckpointsManager.addCheckpoint(checkpoint);
                    }
                    mCheckpointListRecyclerAdapter.notifyDataSetChanged();
                    mProgressDialog.dismiss();
                }catch(JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onDataLoadFailed() {

            }

            @Override
            public void onCanceled() {

            }
        });
        mDataLoader.execute();
    }

    private class CustomSpinnerAdapter extends BaseAdapter {
        ArrayList<String> stringArrayList = new ArrayList<>();
        Context context;

        public CustomSpinnerAdapter(Context context, ArrayList<String> arrayList) {
            this.context = context;
            this.stringArrayList = arrayList;
        }

        @Override
        public int getCount() {
            return stringArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if(view == null) {
                // we'll create a custom view
                view = LayoutInflater.from(context).inflate(R.layout.spinner_basic_layout, parent, false);

                TextView spinnerTextView = (TextView)view.findViewById(R.id.spinner_textview);
                spinnerTextView.setText(stringArrayList.get(position));
                spinnerTextView.setTypeface(MyTypeface.get(context, "clan-book-webfont"));
            }

            return view;
        }
    }
}
