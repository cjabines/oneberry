package com.oneberry.oneberryapp.controller;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.groupm.labs.LocalAppData;
import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.fragments.home.AccountListFragment;
import com.oneberry.oneberryapp.controller.fragments.home.CheckpointListFragment;
import com.oneberry.oneberryapp.controller.fragments.home.SiteListFragment;
import com.oneberry.oneberryapp.controller.fragments.home.TourListFragment;
import com.oneberry.oneberryapp.controller.fragments.home.UserListFragment;
import com.oneberry.oneberryapp.utils.MyTypeface;

/**
 * Created by zechariah.jabines on 3/15/16.
 */
public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    FragmentManager mFragmentManager;
    Toolbar mToolbar;
    TextView mToolbarTextView;
    CardView mCardViewCustomSearch;
    DrawerLayout mDrawer;
    ActionBarDrawerToggle mToggle;
    NavigationView mNavigationView;
    LocalAppData mLocalAppData;

    ImageView mBackButton;
    SearchView mSearchView;

    View mHeaderView;
    TextView mHeaderNameTextView;
    //TextView mHeaderUsernameTextView;

    int mCurrentContent = 0;

    // fragments
    UserListFragment mUserListFragment;
    AccountListFragment mAccountsListFragment;
    SiteListFragment mSiteListFragment;
    CheckpointListFragment mCheckpointListFragment;
    TourListFragment mTourListFragment;
    private NfcAdapter nfcAdapter;
    private PendingIntent nfcPendingIntent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_activity);

        mLocalAppData = LocalAppData.getLocalData(this);

        mToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");

        mToolbarTextView = (TextView)mToolbar.findViewById(R.id.toolbar_textview);
        mToolbarTextView.setText("Tours");
        mToolbarTextView.setTypeface(MyTypeface.get(this, "clan-book-webfont"));

        mCardViewCustomSearch = (CardView)findViewById(R.id.search_layout);
        mSearchView = (SearchView)mCardViewCustomSearch.findViewById(R.id.searchview);
        mBackButton = (ImageView)mCardViewCustomSearch.findViewById(R.id.back_button);

        LinearLayout linearLayout1 = (LinearLayout) mSearchView.getChildAt(0);
        LinearLayout linearLayout2 = (LinearLayout) linearLayout1.getChildAt(2);
        LinearLayout linearLayout3 = (LinearLayout) linearLayout2.getChildAt(1);
        AutoCompleteTextView autoComplete = (AutoCompleteTextView) linearLayout3.getChildAt(0);
        autoComplete.setTypeface(MyTypeface.get(this, "clan-book-webfont"));
        autoComplete.setTextSize(16);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mToggle = new ActionBarDrawerToggle(this, mDrawer, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(mToggle);
        mToggle.syncState();

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        mNavigationView.setCheckedItem(R.id.tours);

        // Header
        mHeaderView = mNavigationView.getHeaderView(0);
        mHeaderNameTextView = (TextView)mHeaderView.findViewById(R.id.name_textview);
        //mHeaderUsernameTextView = (TextView)mHeaderView.findViewById(R.id.user_type_textview);
        mHeaderNameTextView.setTypeface(MyTypeface.get(this, "clan-med-webfont"));
        //mHeaderUsernameTextView.setTypeface(MyTypeface.get(this, "clan-book-webfont"));

        mFragmentManager = getSupportFragmentManager();

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        setContent(R.id.tours);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        mDrawer.closeDrawers();
        setContent(item.getItemId());
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.action_bar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mDrawer.addDrawerListener(mToggle);
        mCardViewCustomSearch.setVisibility(View.VISIBLE);
        mSearchView.onActionViewExpanded();
        mSearchView.setQuery("", false);
        mSearchView.setFocusable(true);
        mSearchView.setIconified(false);
        mSearchView.requestFocusFromTouch();
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                mCardViewCustomSearch.setVisibility(View.GONE);

                switch (mCurrentContent) {
                    case R.id.users:
                        mUserListFragment.searchUser(query);
                        break;

                    case R.id.accounts:
                        mAccountsListFragment.searchAccount(query);
                        break;

                    case R.id.sites:
                        mSiteListFragment.searchSite(query);
                        break;

                    case R.id.checkpoints:
                        //mCheckpointListFragment.searchSite(query);
                        break;
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCardViewCustomSearch.setVisibility(View.GONE);
            }
        });

        return true;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this).setTitle("Exit").setMessage("Are you sure?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(1);
                    }
                })
                .setNegativeButton("No", null);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        alertDialog.setCancelable(false);
    }

    private void setContent(int content) {
        switch (content) {
            case R.id.users:
                mToolbarTextView.setText("User");
                mSearchView.setQueryHint("Search User");
                // set fragment
                //check if there's a fragment in container
                mUserListFragment =  new UserListFragment();
                mCurrentContent = R.id.users;
                mFragmentManager.beginTransaction().replace(R.id.container, mUserListFragment).commit();
                break;

            case R.id.accounts:
                mToolbarTextView.setText("Accounts");
                mSearchView.setQueryHint("Search Accounts");
                // set fragment
                //check if there's a fragment in container
                mAccountsListFragment =  new AccountListFragment();
                mCurrentContent = R.id.accounts;
                mFragmentManager.beginTransaction().replace(R.id.container, mAccountsListFragment).commit();
                break;

            case R.id.sites:
                mToolbarTextView.setText("Sites");
                mSearchView.setQueryHint("Search Sites");
                // set fragment
                //check if there's a fragment in container
                mSiteListFragment =  new SiteListFragment();
                mCurrentContent = R.id.sites;
                mFragmentManager.beginTransaction().replace(R.id.container, mSiteListFragment).commit();
                break;

            case R.id.checkpoints:
                mToolbarTextView.setText("Checkpoints");
                mSearchView.setQueryHint("Search Checkpoints");
                // set fragment
                //check if there's a fragment in container
                mCheckpointListFragment =  new CheckpointListFragment();
                mCurrentContent = R.id.checkpoints;
                mFragmentManager.beginTransaction().replace(R.id.container, mCheckpointListFragment).commit();
                break;

            case R.id.tours:

                mToolbarTextView.setText("Tours");
                mSearchView.setQueryHint("Search Tours");
                // set fragment
                //check if there's a fragment in container
                mTourListFragment =  new TourListFragment();
                mCurrentContent = R.id.tours;
                mFragmentManager.beginTransaction().replace(R.id.container, mTourListFragment).commit();
                break;

            case R.id.report:
                //Intent intent = new Intent(HomeActivity.this, ReportActivity.class);
                //startActivity(intent);
                break;

            case R.id.events:
                Intent intent = new Intent(HomeActivity.this, EventsActivity.class);
                startActivity(intent);
                break;

            case R.id.logout:
                AlertDialog.Builder builder = new AlertDialog.Builder(this).setTitle("Logout").setMessage("Are you sure you want to logout?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                                mLocalAppData.deleteAllEntryFromSharedKey("login_oneberry");

                                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                                startActivity(intent);

                            }
                        }).setNegativeButton("No", null);
                AlertDialog dialog = builder.create();
                dialog.show();
                dialog.setCancelable(false);
                break;
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        byte[] tagId = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);

        if(tagId != null) {
            new GetSerialTagID(mTourListFragment).execute(tagId);
        }else{
            AlertDialog.Builder builder = new AlertDialog.Builder(this).setTitle("Checkpoint").setMessage("Failed to scan this checkpoint: No ndef message/data")
                    .setPositiveButton("OK", null);
            AlertDialog dialog = builder.create();
            dialog.show();
            dialog.setCancelable(false);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(nfcAdapter != null) {
            disableForegroundMode();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(nfcAdapter != null) {
            enableForegroundMode();
        }
    }

    public void enableForegroundMode() {
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        IntentFilter[] writeTagFilters = new IntentFilter[] {tagDetected, ndefDetected};
        nfcAdapter.enableForegroundDispatch(this, nfcPendingIntent, writeTagFilters, null);
    }

    public void disableForegroundMode() {
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            if (event.getAction() == KeyEvent.ACTION_UP)
            {
                mDrawer.openDrawer(Gravity.LEFT);
                return true;
            }
        }
        return super.onKeyUp(keyCode, event);
    }

    private class GetSerialTagID extends AsyncTask<byte[], String, String> {
        private TourListFragment tourListFragment;
        public GetSerialTagID(TourListFragment tourListFragment) {
            super();
            this.tourListFragment = tourListFragment;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(byte[]... params) {
            byte[] tagId = params[0];
            String hexdump = new String();
            for (int i = 0; i < tagId.length; i++) {
                String x = Integer.toHexString(((int) tagId[i] & 0xff));
                if (x.length() == 1) {
                    x = '0' + x;
                }
                hexdump += x;
            }
            return hexdump;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            Log.d("onPostExecute", "onPostExecute: " + s);
            this.tourListFragment.nfcTagDetected(s);
        }
    }
}