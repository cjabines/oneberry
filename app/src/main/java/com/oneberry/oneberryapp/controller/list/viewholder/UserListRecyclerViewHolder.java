package com.oneberry.oneberryapp.controller.list.viewholder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.UserInfoActivity;
import com.oneberry.oneberryapp.model.User;
import com.oneberry.oneberryapp.utils.MyTypeface;

/**
 * Created by zechariah.jabines on 3/21/16.
 */
public class UserListRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView mUserIDTextView, mNameTextView, mUserTypeTextView;
    int userIndex;
    public static final String USER_INDEX_EXTRA = "com.oneberry.UserListRecyclerViewHolder.USER_INDEX_EXTRA";

    public UserListRecyclerViewHolder(View itemView) {
        super(itemView);

        // bind items
        mUserIDTextView = (TextView)itemView.findViewById(R.id.userid_textview);
        mNameTextView = (TextView)itemView.findViewById(R.id.name_textview);
        mUserTypeTextView = (TextView)itemView.findViewById(R.id.designation_textView);

        mUserIDTextView.setTypeface(MyTypeface.get(itemView.getContext(), "clan-book-webfont"));
        mNameTextView.setTypeface(MyTypeface.get(itemView.getContext(), "clan-med-webfont"));
        mUserTypeTextView.setTypeface(MyTypeface.get(itemView.getContext(), "clan-book-webfont"));

        itemView.setOnClickListener(this);
    }

    public void bindData(User user) {
        mUserIDTextView.setText(user.getUserID());
        mNameTextView.setText(user.getLastName() + ", " +user.getFirstName());
        mUserTypeTextView.setText(user.getDesignation());
        userIndex = user.getUserIndex();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), UserInfoActivity.class);
        intent.putExtra(USER_INDEX_EXTRA, userIndex);
        v.getContext().startActivity(intent);
    }
}
