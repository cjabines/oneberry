package com.oneberry.oneberryapp.controller.list.viewholder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.AccountInfoActivity;
import com.oneberry.oneberryapp.model.Account;
import com.oneberry.oneberryapp.utils.MyTypeface;

/**
 * Created by zechariah.jabines on 3/28/16.
 */
public class AccountListRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView mAccountIDTextView, mCompanyNameTextView, mAddressTextView;
    int mAccountIndex;

    public static final String ACCOUNT_INDEX_EXTRA = "com.oneberry.AccountListRecyclerViewHolder.ACCOUNT_INDEX_EXTRA";

    public AccountListRecyclerViewHolder(View itemView) {
        super(itemView);

        // bind items
        mAccountIDTextView = (TextView)itemView.findViewById(R.id.userid_textview);
        mCompanyNameTextView = (TextView)itemView.findViewById(R.id.name_textview);
        mAddressTextView = (TextView)itemView.findViewById(R.id.designation_textView);

        mAccountIDTextView.setTypeface(MyTypeface.get(itemView.getContext(), "clan-book-webfont"));
        mCompanyNameTextView.setTypeface(MyTypeface.get(itemView.getContext(), "clan-med-webfont"));
        mAddressTextView.setTypeface(MyTypeface.get(itemView.getContext(), "clan-book-webfont"));

        itemView.setOnClickListener(this);
    }

    public void bindData(Account account) {
        mAccountIDTextView.setText(account.getAccountID());
        mCompanyNameTextView.setText(account.getCompanyName());
        mAddressTextView.setText(account.getAddress1());
        mAccountIndex = account.getAccountIndex();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), AccountInfoActivity.class);
        intent.putExtra(ACCOUNT_INDEX_EXTRA, mAccountIndex);
        v.getContext().startActivity(intent);
    }
}
