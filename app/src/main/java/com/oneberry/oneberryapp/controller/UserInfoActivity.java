package com.oneberry.oneberryapp.controller;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.utils.SingleFragmentActivity;

public class UserInfoActivity extends SingleFragmentActivity {

    @Override
    protected Fragment createFragment() {
        return new UserInfoFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
