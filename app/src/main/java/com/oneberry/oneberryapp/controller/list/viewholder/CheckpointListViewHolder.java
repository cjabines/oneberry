package com.oneberry.oneberryapp.controller.list.viewholder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.CheckpointInfoActivity;
import com.oneberry.oneberryapp.model.Checkpoint;
import com.oneberry.oneberryapp.utils.MyTypeface;

/**
 * Created by zechariah.jabines on 3/29/16.
 */
public class CheckpointListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    // ui
    private TextView mCheckpointBranchID, mCheckpointName, mCheckpointType;
    private int checkpointIndex;
    private View mItemView;

    // extra
    public static final String EXTRA_CHECKPOINT_INDEX = "CheckpointListViewHolder.EXTRA_CHECKPOINT_INDEX";

    public CheckpointListViewHolder(View itemView) {
        super(itemView);
        mItemView = itemView;

        // bind ui
        mCheckpointBranchID = (TextView)itemView.findViewById(R.id.checkpointbranchid);
        mCheckpointName = (TextView)itemView.findViewById(R.id.checkpointname);
        mCheckpointType = (TextView)itemView.findViewById(R.id.checkpointtype);

        // set font
        mCheckpointBranchID.setTypeface(MyTypeface.get(itemView.getContext(), "clan-book-webfont"));
        mCheckpointName.setTypeface(MyTypeface.get(itemView.getContext(), "clan-med-webfont"));
        mCheckpointType.setTypeface(MyTypeface.get(itemView.getContext(), "clan-book-webfont"));

        itemView.setOnClickListener(this);
    }

    public void bindData(Checkpoint checkpoint) {
        mCheckpointBranchID.setText(checkpoint.getBranch());
        mCheckpointName.setText(checkpoint.getCheckpointName());
        mCheckpointType.setText(checkpoint.getCheckpointType());
        checkpointIndex = checkpoint.getCheckpointIndex();

        if(checkpoint.isTagged()) {
            mItemView.setBackgroundColor(0xff0000);
        }else{
            mItemView.setBackgroundColor(0x00ff00);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), CheckpointInfoActivity.class);
        intent.putExtra(EXTRA_CHECKPOINT_INDEX, checkpointIndex);
        v.getContext().startActivity(intent);
    }
}
