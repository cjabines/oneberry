package com.oneberry.oneberryapp.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.groupm.labs.HttpResponseCache;
import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.data.APIManager;
import com.oneberry.oneberryapp.model.managers.CheckpointIDManager;
import com.oneberry.oneberryapp.utils.MyTypeface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 5/25/16.
 */
public class EventsFragment extends Fragment {


    Toolbar mToolbar;
    Spinner mIdSpinner;
    CustomSpinnerAdapter mSpinnerAdapter;
    EditText mEditText;
    Button mButton;
    ArrayList<String> id = new ArrayList<>();
    CheckpointIDManager checkpointIDManager;
    HttpResponseCache mEventsService;

    ProgressDialog mProgressDialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.report_fragment, container, false);

        // ui
        mToolbar = (Toolbar)v.findViewById(R.id.toolbar);
        mIdSpinner = (Spinner)v.findViewById(R.id.reportID_Spinner);
        mEditText = (EditText)v.findViewById(R.id.report_edittext);
        mButton = (Button)v.findViewById(R.id.submit_button);
        mButton.setEnabled(false);

        ((AppCompatActivity)getActivity()).setSupportActionBar(mToolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        checkpointIDManager = CheckpointIDManager.get(getActivity());
        id = checkpointIDManager.getIds();

        // set spinner
        mSpinnerAdapter = new CustomSpinnerAdapter(getActivity(), id);
        mIdSpinner.setAdapter(mSpinnerAdapter);

        // events
        mEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(mEditText.length() > 0 && mIdSpinner.getSelectedItemPosition() != 0) {
                    mButton.setEnabled(true);
                }else{
                    mButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                if(mEditText.length() > 0 && mIdSpinner.getSelectedItemPosition() != 0) {
                    mButton.setEnabled(true);
                }else{
                    mButton.setEnabled(false);
                }
            }
        });

        mIdSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(mEditText.length() > 0 && mIdSpinner.getSelectedItemPosition() != 0) {
                    mButton.setEnabled(true);
                }else{
                    mButton.setEnabled(false);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Sending Event. Please Wait");
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mProgressDialog.show();

                mEventsService = new HttpResponseCache(getActivity(), APIManager.reportEvent("", "", ""), "");
                mEventsService.method = HttpResponseCache.POST_METHOD;
                mEventsService.addParameter(new Pair<String, String>("formdata", mEditText.getText().toString()));
                mEventsService.addParameter(new Pair<String, String>("user", APIManager.userID));
                mEventsService.addParameter(new Pair<String, String>("reportid", mIdSpinner.getSelectedItem().toString()));
                mEventsService.setWillAddToCache(false);
                mEventsService.execute();
                mEventsService.setObjectListener(new HttpResponseCache.OnDataLoaderListener() {
                    @Override
                    public void onDataLoadComplete(JSONObject jsonObject) {
                        JSONArray checkpointArray = null;
                        try {
                            checkpointArray = jsonObject.getJSONArray("data");
                            JSONObject object = checkpointArray.getJSONObject(0);
                            String success = object.getString("success");
                            if(success.equals("'true'")) {
                                mProgressDialog.dismiss();
                                mEditText.setText("");
                                mIdSpinner.setSelection(0, true);

                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                                        .setPositiveButton("OK", null)
                                        .setTitle("Success")
                                        .setMessage("Event sent");

                                AlertDialog dialog = builder.create();
                                dialog.show();

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onDataLoadFailed() {

                    }

                    @Override
                    public void onCanceled() {

                    }
                });
            }
        });

        return v;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == android.R.id.home) {
            getActivity().finish();
        }
        return true;
    }

    private class CustomSpinnerAdapter extends BaseAdapter {

        private ArrayList<String> id = new ArrayList<>();
        private Context context;

        public CustomSpinnerAdapter(Context context, ArrayList<String> id) {
            super();
            this.context = context;
            this.id = id;
        }

        @Override
        public int getCount() {
            return id.size();
        }

        @Override
        public Object getItem(int position) {
            return id.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = LayoutInflater.from(this.context).inflate(R.layout.spinner_basic_layout, parent, false);
            TextView spinnerTextView = (TextView)convertView.findViewById(R.id.spinner_textview);
            spinnerTextView.setText(id.get(position));
            spinnerTextView.setTypeface(MyTypeface.get(context, "clan-book-webfont"));
            return convertView;
        }
    }
}
