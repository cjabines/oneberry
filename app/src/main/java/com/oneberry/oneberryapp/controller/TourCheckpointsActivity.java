package com.oneberry.oneberryapp.controller;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.oneberry.oneberryapp.utils.SingleFragmentActivity;

/**
 * Created by zechariah.jabines on 3/31/16.
 */
public class TourCheckpointsActivity extends SingleFragmentActivity {

    TourCheckpointsFragment mTourCheckpointsFragment;
    private String TAG = "CHICO";

    // NFC
    public static final String MIME_TEXT_PLAIN = "application/json";
    protected NfcAdapter nfcAdapter;
    protected PendingIntent nfcPendingIntent;

    @Override
    protected Fragment createFragment() {
        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        nfcPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

        mTourCheckpointsFragment = new TourCheckpointsFragment();
        return mTourCheckpointsFragment;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Tag myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);

        // get NDEF tag details
        Ndef ndefTag = Ndef.get(myTag);
        int size = ndefTag.getMaxSize();         // tag size
        boolean writable = ndefTag.isWritable(); // is tag writable?
        String type = ndefTag.getType();         // tag type

        // get NDEF message details
        NdefMessage ndefMesg = ndefTag.getCachedNdefMessage();
        NdefRecord[] ndefRecords = ndefMesg.getRecords();
        String ndefID = new String(ndefRecords[0].getId());

        if(ndefID != null) {
            mTourCheckpointsFragment.nfcTagDetected(ndefID);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(nfcAdapter != null) {
            disableForegroundMode();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(nfcAdapter != null) {
            enableForegroundMode();
        }
    }

    public void enableForegroundMode() {
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        IntentFilter nDefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
        IntentFilter[] writeTagFilters = new IntentFilter[] {tagDetected, nDefDetected};
        nfcAdapter.enableForegroundDispatch(this, nfcPendingIntent, writeTagFilters, null);
    }

    public void disableForegroundMode() {
        nfcAdapter.disableForegroundDispatch(this);
    }
}
