package com.oneberry.oneberryapp.controller;

import android.support.v4.app.Fragment;

import com.oneberry.oneberryapp.utils.SingleFragmentActivity;

/**
 * Created by zechariah.jabines on 5/25/16.
 */
public class EventsActivity extends SingleFragmentActivity {
    @Override
    protected Fragment createFragment() {
        return new EventsFragment();
    }
}
