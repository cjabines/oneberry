package com.oneberry.oneberryapp.controller.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.viewholder.UserListRecyclerViewHolder;
import com.oneberry.oneberryapp.model.User;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/21/16.
 */
public class UserListRecyclerAdapter extends RecyclerView.Adapter<UserListRecyclerViewHolder> {

    private ArrayList<User> mUserList = new ArrayList<>();

    public UserListRecyclerAdapter(ArrayList<User> userArrayList) {
        super();
        mUserList = userArrayList;
    }

    @Override
    public void onBindViewHolder(UserListRecyclerViewHolder holder, int position) {
        holder.bindData(mUserList.get(position));
    }

    @Override
    public UserListRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item_layout, parent, false);
        return new UserListRecyclerViewHolder(view);
    }

    @Override
    public int getItemCount() {
        Log.d("getItemCount", "getItemCount: " + mUserList.size());
        return mUserList.size();
    }
}
