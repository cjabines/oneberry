package com.oneberry.oneberryapp.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.groupm.labs.LocalAppData;
import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.data.APIManager;

/**
 * Created by zechariah.jabines on 3/23/16.
 */
public class SplashFragment extends Fragment {

    TextView mSplashText;
    LocalAppData mLocalAppData;
    boolean hasData = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocalAppData = LocalAppData.getLocalData(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.splash_fragment, container, false);

        //mSplashText = (TextView)view.findViewById(R.id.intro_textview);
        //mSplashText.setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));

        //check if we have a login data
        if(mLocalAppData.hasKey("userID", "login_oneberry") && mLocalAppData.hasKey("userType", "login_oneberry") && mLocalAppData.hasKey("userIMEI", "login_oneberry") && mLocalAppData.hasKey("userCanScan", "login_oneberry")) {
            hasData = true;
            APIManager.userType = mLocalAppData.getValueForKey("userType", "login_oneberry");
            APIManager.userIMEI = mLocalAppData.getValueForKey("userIMEI", "login_oneberry");
            APIManager.userCanScan = mLocalAppData.getValueForKey("userCanScan", "login_oneberry");
            APIManager.userID = mLocalAppData.getValueForKey("userID", "login_oneberry");
        }else{
            // get IMEI number
            TelephonyManager telephonyManager = (TelephonyManager)getActivity().getSystemService(Context.TELEPHONY_SERVICE);
            APIManager.userIMEI = telephonyManager.getDeviceId();
            mLocalAppData.setValueForKey("login_oneberry", "userIMEI", APIManager.userIMEI);
        }

        CountDownTimer countDownTimer = new CountDownTimer(2000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent intent;
                if(hasData) {
                    intent = new Intent(getActivity(), HomeActivity.class);
                }else{
                    intent = new Intent(getActivity(), LoginActivity.class);
                }

                getActivity().startActivity(intent);
                getActivity().finish();
            }
        };
        countDownTimer.start();
        return view;
    }
}
