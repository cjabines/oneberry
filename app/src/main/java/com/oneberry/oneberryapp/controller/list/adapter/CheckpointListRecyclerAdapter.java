package com.oneberry.oneberryapp.controller.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.viewholder.AccountListRecyclerViewHolder;
import com.oneberry.oneberryapp.controller.list.viewholder.CheckpointListViewHolder;
import com.oneberry.oneberryapp.model.Checkpoint;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/29/16.
 */
public class CheckpointListRecyclerAdapter extends RecyclerView.Adapter<CheckpointListViewHolder> {

    private ArrayList<Checkpoint> mCheckpoints = new ArrayList<>();

    public CheckpointListRecyclerAdapter(ArrayList<Checkpoint> arrayList) {
        super();
        mCheckpoints = arrayList;
    }

    @Override
    public CheckpointListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkpoint_list_item_layout, parent, false);
        return new CheckpointListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CheckpointListViewHolder holder, int position) {
        holder.bindData(mCheckpoints.get(position));
    }

    @Override
    public int getItemCount() {
        return mCheckpoints.size();
    }
}
