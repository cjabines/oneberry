package com.oneberry.oneberryapp.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.viewholder.SiteListRecyclerViewHolder;
import com.oneberry.oneberryapp.model.Site;
import com.oneberry.oneberryapp.model.managers.SitesManager;
import com.oneberry.oneberryapp.utils.MyTypeface;

/**
 * Created by zechariah.jabines on 3/29/16.
 */
public class SiteInfoFragment extends Fragment {

    EditText mAccountIDEditText, mSiteIDEditText, mSiteNameEditText, mSiteTypeEditText, mAddressOneEditText, mAddressTwoEditText, mSiteStateEditText, mSiteCountryEditText, mSiteZipCodeEditText, mSiteNotesEditText, mSiteDateAddedEditText;
    TextView mBasicInformationTextView, mToolbarTextView;
    SitesManager mSitesManager;
    int mSiteIndex;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mSiteIndex = getActivity().getIntent().getIntExtra(SiteListRecyclerViewHolder.EXTRA_SITE_INDEX, 0);
        mSitesManager = SitesManager.getInstance(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.site_info_fragment, container, false);

        // bind ui
        Toolbar toolbar = (Toolbar)view.findViewById(R.id.toolbar);
        mToolbarTextView = (TextView)view.findViewById(R.id.toolbar_textview);
        mToolbarTextView.setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));

        mAccountIDEditText = (EditText)view.findViewById(R.id.siteaccountid);
        mSiteIDEditText = (EditText)view.findViewById(R.id.siteid);
        mSiteNameEditText = (EditText)view.findViewById(R.id.sitename);
        mSiteTypeEditText = (EditText)view.findViewById(R.id.sitetype);
        mAddressOneEditText = (EditText)view.findViewById(R.id.siteaddress1);
        mAddressTwoEditText = (EditText)view.findViewById(R.id.siteaddress2);
        mSiteStateEditText = (EditText)view.findViewById(R.id.sitestate);
        mSiteCountryEditText = (EditText)view.findViewById(R.id.sitecountry);
        mSiteZipCodeEditText = (EditText)view.findViewById(R.id.sitezipcode);
        mSiteNotesEditText = (EditText)view.findViewById(R.id.sitenote);
        mSiteDateAddedEditText = (EditText)view.findViewById(R.id.sitedate);
        mBasicInformationTextView = (TextView)view.findViewById(R.id.basicinfo);

        // set font
        EditText[] editTexts = {mSiteIDEditText, mAccountIDEditText, mSiteCountryEditText, mSiteNameEditText, mSiteTypeEditText, mAddressOneEditText, mAddressTwoEditText, mSiteStateEditText, mSiteCountryEditText, mSiteZipCodeEditText, mSiteNotesEditText, mSiteDateAddedEditText};
        for(int i = 0; i < editTexts.length; i++) {
            editTexts[i].setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));
        }
        mBasicInformationTextView.setTypeface(MyTypeface.get(getActivity(), "clan-med-webfont"));

        // add information
        Site site = mSitesManager.getSiteList().get(mSiteIndex);;
        mAccountIDEditText.setText(site.getAccountID());
        mSiteIDEditText.setText(site.getsiteID());
        mSiteNameEditText.setText(site.getSiteName());
        mSiteTypeEditText.setText(site.getSiteType());
        mAddressOneEditText.setText(site.getAddress1());
        mAddressTwoEditText.setText(site.getAddress2());
        mSiteCountryEditText.setText(site.getCountry());
        mSiteZipCodeEditText.setText(site.getZipCode());
        mSiteNotesEditText.setText(site.getOthers());
        mSiteDateAddedEditText.setText(site.getDateAdded());
        mSiteStateEditText.setText(site.getState());

        // set up actionbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return true;
    }
}
