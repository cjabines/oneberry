package com.oneberry.oneberryapp.controller.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.viewholder.AccountListRecyclerViewHolder;
import com.oneberry.oneberryapp.model.Account;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/21/16.
 */
public class AccountListRecyclerAdapter extends RecyclerView.Adapter<AccountListRecyclerViewHolder> {

    private ArrayList<Account> mAccountList = new ArrayList<>();

    public AccountListRecyclerAdapter(ArrayList<Account> accountArrayList) {
        super();
        mAccountList = accountArrayList;
    }

    @Override
    public void onBindViewHolder(AccountListRecyclerViewHolder holder, int position) {
        holder.bindData(mAccountList.get(position));
    }

    @Override
    public AccountListRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.user_list_item_layout, parent, false);
        return new AccountListRecyclerViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return mAccountList.size();
    }
}
