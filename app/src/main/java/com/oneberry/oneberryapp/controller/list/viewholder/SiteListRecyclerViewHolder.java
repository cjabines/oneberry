package com.oneberry.oneberryapp.controller.list.viewholder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.SiteInfoActivity;
import com.oneberry.oneberryapp.model.Site;
import com.oneberry.oneberryapp.utils.MyTypeface;

/**
 * Created by zechariah.jabines on 3/29/16.
 */
public class SiteListRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView mSiteIdTextView, mSiteNameTextView, mSiteAddressTextView;
    int mSiteIndex;
    public static final String EXTRA_SITE_INDEX = "com.oneberry.SiteListRecyclerViewHolder.EXTRA_SITE_INDEX";

    public SiteListRecyclerViewHolder(View itemView) {
        super(itemView);

        mSiteIdTextView = (TextView)itemView.findViewById(R.id.siteid);
        mSiteNameTextView = (TextView)itemView.findViewById(R.id.sitename);
        mSiteAddressTextView = (TextView)itemView.findViewById(R.id.siteaddress);

        mSiteIdTextView.setTypeface(MyTypeface.get(itemView.getContext(), "clan-book-webfont"));
        mSiteNameTextView.setTypeface(MyTypeface.get(itemView.getContext(), "clan-med-webfont"));
        mSiteAddressTextView.setTypeface(MyTypeface.get(itemView.getContext(), "clan-book-webfont"));

        itemView.setOnClickListener(this);
    }

    public void bindData(Site site) {
        mSiteIdTextView.setText(site.getsiteID());
        mSiteNameTextView.setText(site.getSiteName());
        mSiteAddressTextView.setText(site.getAddress1());
        mSiteIndex = site.getSiteIndex();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), SiteInfoActivity.class);
        intent.putExtra(EXTRA_SITE_INDEX, mSiteIndex);
        v.getContext().startActivity(intent);
    }
}
