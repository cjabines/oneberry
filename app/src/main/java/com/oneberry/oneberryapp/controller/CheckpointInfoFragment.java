package com.oneberry.oneberryapp.controller;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.groupm.labs.HttpResponseCache;
import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.viewholder.ToursCheckpointListRecyclerViewHolder;
import com.oneberry.oneberryapp.data.APIManager;
import com.oneberry.oneberryapp.model.Checkpoint;
import com.oneberry.oneberryapp.model.managers.CheckpointsManager;
import com.oneberry.oneberryapp.utils.MyTypeface;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zechariah.jabines on 3/30/16.
 */
public class CheckpointInfoFragment extends Fragment {

    TextView mBasicInfoTextView, mToolbarTextView;
    EditText mBranchIDEditText, mTagIDEditText, mCheckpointNameEditText, mCheckpointTypeEditText, mCheckpointLocationEditText, mCheckpointNotesEditText;
    int mCheckpointIndex;
    String mTourTagID;
    CheckpointsManager mCheckpointsManager;
    HttpResponseCache mResponseCache;

    // simulate tagging
    ProgressDialog mProgressDialog;
    Checkpoint mCheckpoint;

    boolean isScanning = false;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mCheckpointsManager = CheckpointsManager.getInstance(getActivity());
        mCheckpointIndex = getActivity().getIntent().getIntExtra(ToursCheckpointListRecyclerViewHolder.EXTRA_CHECKPOINT_TOUR_INDEX, 0);
        mTourTagID = getActivity().getIntent().getStringExtra(ToursCheckpointListRecyclerViewHolder.EXTRA_TOUR_ID);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Checking in... Please wait");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.checkpoint_info_fragment, container, false);

        // bind ui
        Toolbar toolbar = (Toolbar)view.findViewById(R.id.toolbar);
        mToolbarTextView = (TextView)view.findViewById(R.id.toolbar_textview);
        mToolbarTextView.setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));

        mBasicInfoTextView = (TextView)view.findViewById(R.id.basicinfo);
        mBranchIDEditText = (EditText)view.findViewById(R.id.checkpointbranchid);
        mTagIDEditText = (EditText)view.findViewById(R.id.checkpointtagid);
        mCheckpointTypeEditText = (EditText)view.findViewById(R.id.checkpointtype);
        mCheckpointLocationEditText = (EditText)view.findViewById(R.id.checkpointlocation);
        mCheckpointNotesEditText = (EditText)view.findViewById(R.id.checkpointnote);

        // set font
        EditText[] editTexts = {mBranchIDEditText, mTagIDEditText, mCheckpointNameEditText, mCheckpointTypeEditText, mCheckpointLocationEditText, mCheckpointNotesEditText};
        for(int i = 0; i < editTexts.length; i++) {
            editTexts[i].setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));
        }
        mBasicInfoTextView.setTypeface(MyTypeface.get(getActivity(), "clan-med-webfont"));

        // set data
        mCheckpoint = mCheckpointsManager.getCheckpointList().get(mCheckpointIndex);
        mBranchIDEditText.setText(mCheckpoint.getBranch());
        mTagIDEditText.setText(mCheckpoint.getTagID());
        mCheckpointNameEditText.setText(mCheckpoint.getCheckpointName());
        mCheckpointTypeEditText.setText(mCheckpoint.getCheckpointType());
        mCheckpointLocationEditText.setText(mCheckpoint.getLocation());
        mCheckpointNotesEditText.setText(mCheckpoint.getNotes());

        // set up actionbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return true;
    }


    public void nfcTagDetected() {
        /*if(!isScanning) {
            isScanning = true;

            mProgressDialog.show();
            mResponseCache = new HttpResponseCache(getActivity(), APIManager.reportCheckpoint(APIManager.userID, mTourTagID, mCheckpoint.getTagID()), "");
            mResponseCache.setWillAddToCache(false);
            mResponseCache.setObjectListener(new HttpResponseCache.OnDataLoaderListener() {
                @Override
                public void onDataLoadComplete(JSONObject jsonObject) {
                    mProgressDialog.dismiss();

                    Log.d("onDataLoadComplete", "onDataLoadComplete: " + jsonObject);
                    try {
                        JSONArray checkpointArray = jsonObject.getJSONArray("data");
                        JSONObject checkpointObject = checkpointArray.getJSONObject(0);
                        String checkID = checkpointObject.getString("checkpointid");
                        String success = checkpointObject.getString("success");
                        if(success.equals("true")) {
                            if(checkID.equals("")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Checkpoint").setMessage("Scan complete. Please proceed to the next checkpoint")
                                        .setPositiveButton("OK", null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                dialog.setCancelable(false);
                            }else{
                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Checkpoint").setMessage("Failed to add new checkpoint. Please try again")
                                        .setPositiveButton("OK", null);
                                AlertDialog dialog = builder.create();
                                dialog.show();
                                dialog.setCancelable(false);
                            }
                        }
                    }catch (JSONException e) {
                        e.printStackTrace();
                    }
                    isScanning = false;
                }

                @Override
                public void onDataLoadFailed() {

                }

                @Override
                public void onCanceled() {

                }
            });
            mResponseCache.execute();
        }*/
    }

}
