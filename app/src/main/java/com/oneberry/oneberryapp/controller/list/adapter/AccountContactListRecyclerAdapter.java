package com.oneberry.oneberryapp.controller.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.viewholder.AccountContactListViewHolder;
import com.oneberry.oneberryapp.model.AccountContact;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/29/16.
 */
public class AccountContactListRecyclerAdapter extends RecyclerView.Adapter<AccountContactListViewHolder> {

    ArrayList<AccountContact> mAccountContacts = new ArrayList<>();

    public AccountContactListRecyclerAdapter(ArrayList<AccountContact> arrayList) {
        super();
        mAccountContacts = arrayList;
    }

    @Override
    public AccountContactListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.account_contact_info_list_layout, parent, false);
        return new AccountContactListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AccountContactListViewHolder holder, int position) {
        holder.bindData(mAccountContacts.get(position));
    }

    @Override
    public int getItemCount() {
        return mAccountContacts.size();
    }
}
