package com.oneberry.oneberryapp.controller.list.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.viewholder.SiteListRecyclerViewHolder;
import com.oneberry.oneberryapp.model.Site;

import java.util.ArrayList;

/**
 * Created by zechariah.jabines on 3/29/16.
 */
public class SiteListRecyclerAdapter extends RecyclerView.Adapter<SiteListRecyclerViewHolder> {

    ArrayList<Site> mSites = new ArrayList<>();

    public SiteListRecyclerAdapter(ArrayList<Site> arrayList) {
        super();
        mSites = arrayList;
    }

    @Override
    public SiteListRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.site_list_layout, parent, false);
        return new SiteListRecyclerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SiteListRecyclerViewHolder holder, int position) {
        holder.bindData(mSites.get(position));
    }

    @Override
    public int getItemCount() {
        return mSites.size();
    }
}
