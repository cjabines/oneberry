package com.oneberry.oneberryapp.controller.list.viewholder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.CheckpointInfoActivity;
import com.oneberry.oneberryapp.model.Checkpoint;
import com.oneberry.oneberryapp.utils.MyTypeface;

/**
 * Created by zechariah.jabines on 3/30/16.
 */
public class ToursCheckpointListRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView mCheckpointIDTextView, mCheckpointNameTextView;
    int mTourIndex;
    View mItemView;
    Checkpoint mCheckpoint;

    public static final String EXTRA_CHECKPOINT_TOUR_INDEX = "ToursCheckpointListRecyclerViewHolder.EXTRA_CHECKPOINT_TOUR_INDEX";
    public static final String EXTRA_TOUR_ID = "ToursCheckpointListRecyclerViewHolder.EXTRA_TOUR_ID";

    public ToursCheckpointListRecyclerViewHolder(View v) {
        super(v);
        mItemView = v;

        // bind ui
        mCheckpointIDTextView = (TextView)v.findViewById(R.id.checkpointid);
        mCheckpointNameTextView = (TextView)v.findViewById(R.id.checkpointname);

        // set font
        mCheckpointIDTextView.setTypeface(MyTypeface.get(v.getContext(), "clan-book-webfont"));
        mCheckpointNameTextView.setTypeface(MyTypeface.get(v.getContext(), "clan-med-webfont"));

        itemView.setOnClickListener(this);
    }

    public void bindData(Checkpoint checkpoint) {
        mCheckpoint = checkpoint;
        mCheckpointIDTextView.setText(checkpoint.getTagID());
        mCheckpointNameTextView.setText(checkpoint.getCheckpointName());
        mTourIndex = checkpoint.getCheckpointIndex();
        if(checkpoint.isTagged()) {
            mItemView.setBackgroundColor(0xFFb1fec1);
        }else{
            mItemView.setBackgroundColor(0xFFfedfb1);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), CheckpointInfoActivity.class);
        intent.putExtra(EXTRA_CHECKPOINT_TOUR_INDEX, mTourIndex);
        intent.putExtra(EXTRA_TOUR_ID, mCheckpoint.getTagID());
        v.getContext().startActivity(intent);
    }
}
