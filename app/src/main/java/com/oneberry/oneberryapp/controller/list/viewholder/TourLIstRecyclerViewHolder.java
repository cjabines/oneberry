package com.oneberry.oneberryapp.controller.list.viewholder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.TourCheckpointsActivity;
import com.oneberry.oneberryapp.model.Tour;
import com.oneberry.oneberryapp.utils.MyTypeface;

/**
 * Created by zechariah.jabines on 3/30/16.
 */
public class TourListRecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView mTourIDTextView, mTourNameTextView, mTourTypeTextView;
    int mTourIndex;

    public static final String EXTRA_TOUR_INDEX = "TourListRecyclerViewHolder.EXTRA_TOUR_INDEX";

    public TourListRecyclerViewHolder(View v) {
        super(v);

        // bind ui
        mTourIDTextView = (TextView)v.findViewById(R.id.tourid);
        mTourNameTextView = (TextView)v.findViewById(R.id.tourname);
        mTourTypeTextView = (TextView)v.findViewById(R.id.tourtype);

        // set font
        mTourTypeTextView.setTypeface(MyTypeface.get(v.getContext(), "clan-book-webfont"));
        mTourIDTextView.setTypeface(MyTypeface.get(v.getContext(), "clan-book-webfont"));
        mTourNameTextView.setTypeface(MyTypeface.get(v.getContext(), "clan-med-webfont"));

        itemView.setOnClickListener(this);
    }

    public void bindData(Tour tour) {
        mTourIDTextView.setText(tour.getTourID());
        mTourNameTextView.setText(tour.getGuardTourName());
        mTourTypeTextView.setText(tour.getTourType());
        mTourIndex = tour.getIndex();
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(v.getContext(), TourCheckpointsActivity.class);
        intent.putExtra(EXTRA_TOUR_INDEX, mTourIndex);
        v.getContext().startActivity(intent);
    }
}
