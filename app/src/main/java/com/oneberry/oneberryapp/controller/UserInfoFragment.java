package com.oneberry.oneberryapp.controller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.oneberry.oneberryapp.R;
import com.oneberry.oneberryapp.controller.list.viewholder.UserListRecyclerViewHolder;
import com.oneberry.oneberryapp.model.User;
import com.oneberry.oneberryapp.model.managers.UsersManager;
import com.oneberry.oneberryapp.utils.MyTypeface;

/**
 * Created by zechariah.jabines on 3/23/16.
 */
public class UserInfoFragment extends Fragment {

    //ui
    TextView mToolbarTextView, mPersonalInformationTextView, mContactInformationTextView;
    EditText mLastNameEditText, mFirstNameEditText, mUsernameEditText, mDesignationEditText, mEmailEditText, mDirectlineEditText, mDirectlineExtEditText, mMobileNumberEditText, mOtherPhoneEditText, mSkypeEditText, mNotesEditText;

    // data
    private int mUserIndex;
    private UsersManager mUsersManager;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUserIndex = getActivity().getIntent().getIntExtra(UserListRecyclerViewHolder.USER_INDEX_EXTRA, 0);
        mUsersManager = UsersManager.getInstance(getActivity());
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_info_fragment, container, false);

        // bind ui
        Toolbar toolbar = (Toolbar)view.findViewById(R.id.toolbar);
        mToolbarTextView = (TextView)view.findViewById(R.id.toolbar_textview);
        mToolbarTextView.setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));

        mPersonalInformationTextView = (TextView)view.findViewById(R.id.personal);
        mPersonalInformationTextView.setTypeface(MyTypeface.get(getActivity(), "clan-med-webfont"));

        mContactInformationTextView = (TextView)view.findViewById(R.id.contactinfo);
        mContactInformationTextView.setTypeface(MyTypeface.get(getActivity(), "clan-med-webfont"));

        mLastNameEditText = (EditText)view.findViewById(R.id.lastname);
        mFirstNameEditText = (EditText)view.findViewById(R.id.firstname);
        mUsernameEditText = (EditText)view.findViewById(R.id.username);
        mDesignationEditText = (EditText)view.findViewById(R.id.designation);
        mEmailEditText = (EditText)view.findViewById(R.id.email);
        mDirectlineEditText = (EditText)view.findViewById(R.id.directline);
        mDirectlineExtEditText = (EditText)view.findViewById(R.id.directlineext);
        mMobileNumberEditText = (EditText)view.findViewById(R.id.mobile);
        mOtherPhoneEditText = (EditText)view.findViewById(R.id.otherphone);
        mSkypeEditText = (EditText)view.findViewById(R.id.skype);
        mNotesEditText = (EditText)view.findViewById(R.id.note);
        EditText[] eArray = {mLastNameEditText, mFirstNameEditText, mUsernameEditText, mDesignationEditText, mEmailEditText,
                                mDirectlineEditText, mDirectlineExtEditText, mMobileNumberEditText, mOtherPhoneEditText, mSkypeEditText,
                                mNotesEditText};

        for(int i = 0; i < eArray.length; i++) {
            eArray[i].setTypeface(MyTypeface.get(getActivity(), "clan-book-webfont"));
        }

        // display data
        User user = mUsersManager.getUserList().get(mUserIndex);
        Log.d("onCreateView", "onCreateView: " + user.getFirstName());
        mLastNameEditText.setText(user.getLastName());
        mFirstNameEditText.setText(user.getFirstName());
        mUsernameEditText.setText(user.getUserName());
        mDesignationEditText.setText(user.getDesignation());
        mEmailEditText.setText(user.getEmailAddress());
        mDirectlineEditText.setText(user.getDirectLine());
        mDirectlineExtEditText.setText(user.getDirectLineExt());
        mMobileNumberEditText.setText(user.getMobileNumber());
        mOtherPhoneEditText.setText(user.getOtherPhone());
        mSkypeEditText.setText(user.getSkype());
        mNotesEditText.setText(user.getNotes());

        // set up actionbar
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle("");
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return view;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            getActivity().finish();
        }
        return true;
    }
}
