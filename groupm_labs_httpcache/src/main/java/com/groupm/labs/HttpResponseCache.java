package com.groupm.labs;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.util.Pair;
import android.util.Log;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zechariah.jabines on 1/20/16.
 *
 * Load from URL then save it shared preferences
 */
public class HttpResponseCache extends AsyncTask<String, String, JSONObject> {
    private OnDataLoaderListener mListener;
    private LocalAppData mLocalData;
    private CountDownTimer mTimeoutTimer;

    public interface OnDataLoaderListener {
        void onDataLoadComplete(JSONObject jsonObject);
        void onDataLoadFailed();
        void onCanceled();
    }

    // Constructor Requirements
    private Context mAppContext; // will use this soon
    private String mURLKey;
    private String mPrefKey;

    // Http Connection
    private HttpURLConnection mHttpURLConnection;
    private InputStream mInputStream;
    private BufferedReader mBufferedReader;
    private JSONObject mJSONObject;

    // Add to cache
    private boolean willAddToCache = true;
    private String mCustomKeyName;

    // method
    public static int GET_METHOD = 0;
    public static int POST_METHOD = 1;
    public int method = GET_METHOD;

    // POST
    private ArrayList<Pair> parameters = new ArrayList<>();

    public void addParameter(Pair<String, String> pair) {
        this.parameters.add(pair);
    }

    public HttpResponseCache(Context context, String url, String cachePrefKey, String... customName) {
        super();
        this.mAppContext = context;
        this.mURLKey = url;
        this.mListener = null;
        this.mLocalData = LocalAppData.getLocalData(mAppContext);
        this.mPrefKey = cachePrefKey;
        if(customName.length > 0) {
            this.mCustomKeyName = customName[0];
        }

        mTimeoutTimer = new CountDownTimer(15000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                // none
            }

            @Override
            public void onFinish() {
                if(mListener != null) {
                    mListener.onCanceled();
                }
            }
        };

        Log.d("HTTP_URL_", url);
    }

    public void setObjectListener(OnDataLoaderListener listener) {
        this.mListener = listener;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        mTimeoutTimer.start();
    }

    @Override
    protected JSONObject doInBackground(String... params) {
        if(method == GET_METHOD) {
            if(willAddToCache) {
                if(mCustomKeyName == null) {
                    if(mLocalData.hasKey(this.mURLKey, this.mPrefKey)) {
                        // we have a key. load from pref data
                        try {
                            mJSONObject = new JSONObject(mLocalData.getValueForKey(this.mURLKey, this.mPrefKey));
                        }catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        // no key. load from URL
                        // check if we have a network connection
                        if(isNetworkAvailable()) {
                            try {
                                URL url = new URL(this.mURLKey);
                                mHttpURLConnection = (HttpURLConnection) url.openConnection();
                                mInputStream = new BufferedInputStream(mHttpURLConnection.getInputStream());
                                mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream));
                                mJSONObject = new JSONObject(mBufferedReader.readLine());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else{
                            //mJSONObject = null;
                            if(this.mListener != null) {
                                mListener.onDataLoadFailed();
                            }
                        }
                    }
                }else{
                    if(mLocalData.hasKey(this.mCustomKeyName, this.mPrefKey)) {
                        // we have a key. load from pref data
                        try {
                            mJSONObject = new JSONObject(mLocalData.getValueForKey(this.mCustomKeyName, this.mPrefKey));
                        }catch (Exception e) {
                            e.printStackTrace();
                        }
                    }else{
                        // no key. load from URL
                        // check if we have a network connection
                        if(isNetworkAvailable()) {
                            try {
                                URL url = new URL(this.mURLKey);
                                mHttpURLConnection = (HttpURLConnection) url.openConnection();
                                mInputStream = new BufferedInputStream(mHttpURLConnection.getInputStream());
                                mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream));
                                mJSONObject = new JSONObject(mBufferedReader.readLine());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }else{
                            //mJSONObject = null;
                            if(this.mListener != null) {
                                mListener.onDataLoadFailed();
                            }
                        }
                    }
                }

                return mJSONObject;
            }else{
                if(isNetworkAvailable()) {
                    try {
                        URL url = new URL(this.mURLKey);
                        mHttpURLConnection = (HttpURLConnection) url.openConnection();
                        mInputStream = new BufferedInputStream(mHttpURLConnection.getInputStream());
                        mBufferedReader = new BufferedReader(new InputStreamReader(mInputStream));
                        mJSONObject = new JSONObject();

                        String line;
                        StringBuilder result = new StringBuilder();
                        while ((line = mBufferedReader.readLine()) != null) {
                            result.append(line);
                        }
                        mJSONObject.put("data", new JSONArray(result.toString()));
                    } catch (Exception e) {
                        if(this.mListener != null) {
                            mListener.onDataLoadFailed();
                        }
                        e.printStackTrace();
                    }
                }else{
                    //mJSONObject = null;
                    if(this.mListener != null) {
                        mListener.onDataLoadFailed();
                    }
                }
                return mJSONObject;
            }
        }else{
            if(isNetworkAvailable()) {
                try {
                    URL url = new URL(this.mURLKey);
                    mHttpURLConnection = (HttpURLConnection)url.openConnection();
                    mHttpURLConnection.setRequestMethod("POST");
                    mHttpURLConnection.setDoInput(true);
                    mHttpURLConnection.connect();

                    mJSONObject = new JSONObject();

                    // add data
                    // after conn.connect()
                    OutputStream os = mHttpURLConnection.getOutputStream();
                    BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
                    writer.write(getQuery(this.parameters));
                    writer.flush();
                    writer.close();
                    os.close();

                    int responseCode = mHttpURLConnection.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        InputStream is = new BufferedInputStream(mHttpURLConnection.getInputStream());
                        BufferedReader br = new BufferedReader(new InputStreamReader(is));
                        String line;
                        StringBuilder result = new StringBuilder();
                        while ((line = br.readLine()) != null) {
                            result.append(line);
                            Log.d("doInBackground", "doInBackground: " + result);
                        }
                        mJSONObject.put("data", new JSONArray(result.toString()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else{
                //mJSONObject = null;
                if(this.mListener != null) {
                    mListener.onDataLoadFailed();
                }
            }
            return mJSONObject;
        }
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        if(jsonObject != null) {
            if(jsonObject.length() > 0) {
                if(this.mListener != null) {
                    // set value for key after successful load
                    if(mCustomKeyName == null) {
                        mLocalData.setValueForKey(this.mPrefKey, this.mURLKey, mJSONObject.toString());
                    }else{
                        mLocalData.setValueForKey(this.mPrefKey, this.mCustomKeyName, mJSONObject.toString());
                    }
                    mTimeoutTimer.cancel();
                    mListener.onDataLoadComplete(jsonObject);
                }
            }else{
                if(this.mListener != null) {
                    mListener.onDataLoadFailed();
                }
            }
        }else{
            if(this.mListener != null) {
                mListener.onDataLoadFailed();
            }
        }
    }

    @Override
    protected void onCancelled(JSONObject jsonObject) {
        super.onCancelled(jsonObject);
        if(this.mListener != null) {
            mListener.onDataLoadFailed();
        }
    }

    private boolean isNetworkAvailable() {
        if(mAppContext != null) {
            ConnectivityManager connectivityManager = (ConnectivityManager)mAppContext.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }else {
            return false;
        }
    }

    public void setWillAddToCache(boolean willAddToCache) {
        this.willAddToCache = willAddToCache;
    }

    private String getQuery(List<Pair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Pair pair : params)
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(pair.first.toString(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(pair.second.toString(), "UTF-8"));
        }
        Log.d("getQuery", "getQuery: " + result.toString());
        return result.toString();
    }
}
